﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class LimitedValue : MonoBehaviour
{
    public event Action<int, int> OnChange;
    public event Action<bool> OnReachLimit;
    public event Action OnTryOverLimit;

    private bool _isInit;
    private int _currentValue;
    private int _limitValue;

    // Init() - is parody on ctor, bcs MonoBehaviour enjoyer.
    public void Init() => Init(0, 10);
    public void Init(int currentValue, int limitValue)
    {
        if (_isInit == true) return;

        if (currentValue < 0)
            throw new ArgumentException($"Current Value not be below 0");
        if (limitValue < 0)
            throw new ArgumentException($"Limit Value not be below 0");
        if (limitValue < currentValue)
            throw new ArgumentException($"Limit Value not be below Current Value");

        _currentValue = currentValue;
        _limitValue = limitValue;

        _isInit = true;

        ChangeOn(0);
    }

    private bool IsValidated()
    {
        if (_isInit == false)
            throw new Exception($"Limited Value not initialized. Before use, you must call Init() method.");

        return true;
    }

    public bool IsReachedLimit() => _currentValue == _limitValue;
    public bool TryIncrease()
    {
        IsValidated();

        if (IsReachedLimit() == true)
        {
            OnTryOverLimit?.Invoke();
            return false;
        }

        ChangeOn(1);
        return true;
    }
    public bool TryRemove()
    {
        IsValidated();

        if (_currentValue == 0) return false;

        ChangeOn(-1);
        return true;
    }
    public void IncreaseLimit()
    {
        IsValidated();

        _limitValue++;
        ChangeOn(0);
    }

    private void ChangeOn(int value)
    {
        _currentValue += value;

        OnChange?.Invoke(_currentValue, _limitValue);
        OnReachLimit?.Invoke(IsReachedLimit());
    }

}
