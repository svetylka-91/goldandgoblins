﻿using System;
using UnityEngine;

[Serializable]
public struct KeyValuePair<K, V>
{
    [field: SerializeField] public K Key { get; private set; }
    [field: SerializeField] public V Value { get; private set; }
}