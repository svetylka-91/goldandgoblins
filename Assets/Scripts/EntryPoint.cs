using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    public static EntryPoint Instance;

    public CurrencyManager CurrencyManager;
    public ChestsManager ChestsManager;
    public GoblinsManager GoblinsManager;
    public CardsManager CardsManager;
    public MineSidesManager MineSidesManager;
    public AdsManager AdsManager;
    public SaveLoadManager SaveLoadManager;

    public LevelsManager LevelsManager;
    public PicableResourcesManager PicableResourcesManager;

    public UIManager UIManager;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;


        SaveLoadManager = new SaveLoadManager();
        CurrencyManager = new CurrencyManager();
        GoblinsManager = new GoblinsManager();
        CardsManager = new CardsManager();
        MineSidesManager = new MineSidesManager();
        AdsManager = new AdsManager();
        

        LevelsManager = Instantiate(LevelsManager);
        PicableResourcesManager = Instantiate(PicableResourcesManager);

        UIManager.Init();
        LevelsManager.Init();
        ChestsManager.Init();

        //emitter counts after all inits
        CurrencyManager.Coins.ChangeCount(20);
        CurrencyManager.Bottles.ChangeCount(50);
        CurrencyManager.Gems.ChangeCount(30000);

        SaveLoadManager.MakeLoad();

        StartCoroutine(SaveLoadManager.RegularSaving());
    }
    private void OnDestroy()
    {
        CardsManager.OnDestroy();
    }
    void Update()
    {
        GoblinsManager.MakeWorks();
    }
}
