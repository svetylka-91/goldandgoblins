﻿using UnityEngine;
using System.Collections;

public class MineSideCoin : MonoBehaviour
{
    [SerializeField] private MineSide _mineSide;
    [SerializeField] private ParticleSystem _particle;
    [SerializeField] private AnimatorFlightResources _animatorFlightGold;
    [SerializeField] private Transform _cartPayload;

    private void OnMouseDown()
    {
        Vector2 goldStackPosition = RectTransformUtility.WorldToScreenPoint(CameraInstance.Instance.MyCamera, _cartPayload.transform.position);
        _animatorFlightGold.SpawnStackGold(goldStackPosition);

        _mineSide.AddCoins();
        _particle.Play();
    }
}
