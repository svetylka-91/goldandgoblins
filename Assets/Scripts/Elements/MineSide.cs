﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;
using UnityEngine.UI;

public class MineSide : MonoBehaviour
{

    [SerializeField] private GameObject _inactiveMine;
    [SerializeField] private ButtonForUpgrade _buttonForUpgrade;

    [SerializeField] private GameObject _coinGO;
    [SerializeField] private GameObject _upperCircle;
    private CircleAndText _topCircleAndText;
    [SerializeField] private GameObject _topTextGo;
    [SerializeField] private TextMeshProUGUI _topText;

    [SerializeField] private SleighDriveMine _sleighHandler;

    [SerializeField] private DestroyableObject _destroyableObject;


    [Header("Automatization")]

    [SerializeField] private List<Sprite> _listImageButton;
    [SerializeField] private Button _buttonForAuto;
    [SerializeField] private GameObject _checkForAuto;
    [Header("Activation")]
    [SerializeField] private GameObject _canvasActivation;
    [SerializeField] private Button _btnActivation;


    [Header("Gem part")]
    [SerializeField] private SpriteRenderer _gemSprite;
    [SerializeField] private List<Sprite> _listImageTypeCard;
    [SerializeField] private Gems _typeMain = Gems.amethist;

    private TimerForCircle _timerForMining;
    private bool IsAutomationIncome = false;
    private bool _cartAnimationBegining = false;
    private float _speed = 7.5f;

    private MineSidesManager _manager;

    private void Awake()
    {
        _manager = EntryPoint.Instance.MineSidesManager;

        _destroyableObject.ActionWhenDurabilityIsOver += IsClearedFromRocks;
        EntryPoint.Instance.CardsManager.OnChangeCountCardsPosibleUp += CheckPosibleForAuto;
        _btnActivation.onClick.AddListener(Activation);
        _buttonForAuto.onClick.AddListener(OnClickAuto);

        _gemSprite.sprite = _listImageTypeCard[(int)_typeMain];

        _inactiveMine.SetActive(true);
        _canvasActivation.SetActive(false);
        _buttonForAuto.gameObject.SetActive(false);

        _buttonForUpgrade.gameObject.SetActive(false);
        _buttonForUpgrade.SetParams(
            Balance.SideMineStartPrice(_typeMain), 
            Balance.SideMineMultyPricePerLVL(_typeMain), 
            Balance.MineSideBorderValues[_typeMain]);
        _buttonForUpgrade.SetCalback(OnSetLevel);

        _speed = Balance.SideMineSpeed(_typeMain);

        _topCircleAndText = _upperCircle.GetComponent<CircleAndText>();
        _timerForMining = gameObject.AddComponent<TimerForCircle>();
        _timerForMining.InitTimerForCircle(_speed, new Dictionary<double, Action> { { 0, OnTimerMakeMoney }, { 1, OnTimerStartCartAnimation } }, _topCircleAndText);
        _timerForMining.Pause();

        _sleighHandler.SpawnNewSleigh();


        EntryPoint.Instance.MineSidesManager.AddMineFromScene(_typeMain, this);
    }

    private void OnDestroy()
    {
        EntryPoint.Instance.CardsManager.OnChangeCountCardsPosibleUp -= CheckPosibleForAuto;
        _destroyableObject.ActionWhenDurabilityIsOver -= IsClearedFromRocks;
        _btnActivation.onClick.RemoveAllListeners();
        _buttonForAuto.onClick.RemoveAllListeners();
    }

    private void OnTimerMakeMoney()
    {
        if (EntryPoint.Instance.MineSidesManager.AllMineSideData[_typeMain].IsAuto)
        {
            _cartAnimationBegining = false;
            EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(Balance.SideMineIncomePerTime(_typeMain));
        }
        else
        {
            _timerForMining.Pause();
            ShowCoinForManualGetting();
        }
    }

    private void OnTimerStartCartAnimation()
    {
        if (_cartAnimationBegining) return;

        _cartAnimationBegining = true;
        //TODO старт анимации тележки
        _sleighHandler.MoveToEndPosition();
    }

    private void Activation()
    {
        _canvasActivation.SetActive(false);
        _upperCircle.SetActive(true);
        _topTextGo.SetActive(true);
        _buttonForAuto.gameObject.SetActive(true);
        _buttonForUpgrade.gameObject.SetActive(true);
        _timerForMining.Unpause();

        EntryPoint.Instance.MineSidesManager.AllMineSideData[_typeMain].IsAuto = false;
        OnSetLevel(1);

        EntryPoint.Instance.UIManager._uiMineChest.GenerateChest(ChestType.Wood);
        EntryPoint.Instance.UIManager._uiMineChest.SetTopText(EntryPoint.Instance.MineSidesManager.GetCaptionBeGem(_typeMain));
    }

    private void CheckPosibleForAuto(int i)
    {
        DemandsCardForAuto demandCard = EntryPoint.Instance.MineSidesManager.GetDemandsCardForAuto(_typeMain);
        if (demandCard.Level <= demandCard.Card.Level)
            _buttonForAuto.image.sprite = _listImageButton[0];
        else
            _buttonForAuto.image.sprite = _listImageButton[1];
    }
    private void OnClickAuto()
    {
        EntryPoint.Instance.UIManager.GetPanelMineAutomatization().GenerateScreen(_typeMain);
    }

    public void SetAutomatization ()
    {
        _buttonForAuto.gameObject.SetActive(false);
        _checkForAuto.SetActive(true);
        EntryPoint.Instance.MineSidesManager.AllMineSideData[_typeMain].IsAuto = true;
    }

    private void ShowCoinForManualGetting()
    {
        _coinGO.SetActive(true);
    }
    public void AddCoins()
    {
        _sleighHandler.StartTracing();
        _sleighHandler.SpawnNewSleigh();

        _cartAnimationBegining = false;
        _coinGO.SetActive(false);
        EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(Balance.SideMineIncomePerTime(_typeMain));
        _timerForMining.Unpause();


    }

    public void OnSetLevel(int newLevel)
    {
        _buttonForUpgrade.SetLevel(newLevel);
        _manager.SetLevel(_typeMain, newLevel);
        _topText.text = SpecialFunctions.GetStringFromBigDouble(Balance.SideMineIncomePerTime(_typeMain));
    }

    private void IsClearedFromRocks()
    {
        EffectsAudioManager.PlaySoundClearingRubble();
        _inactiveMine.SetActive(false);
        _canvasActivation.SetActive(true);

    }

}
