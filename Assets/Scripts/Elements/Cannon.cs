using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Cannon : MonoBehaviour
{
    [SerializeField] private GameObject _circleProgressBar;
    [SerializeField] private CircleAndText circleAndText;
    [SerializeField] private UIBottomPanel _uiBottomPanel;
    [SerializeField] private TMPro.TMP_Text _lvlText;

    private TimerForCircle _timerForFastShot;
    private TimerForCircle _mineTimer;

    [SerializeField]
    private float _speed = 1800f;
    private float _speedFastShot;
    
    
    [SerializeField]
    private GameObject _canon;
    [SerializeField] Transform _forParticleCreate;
    [SerializeField] ParticleSystem _puff;
    [SerializeField] ParticleSystem _boom;
    
    private void Start()
    {
        _speed = Balance.CannonSpeed;
        _speedFastShot = Balance.CannonSpeedFastShot;
        
        SetCannonLevel(1);
        StartCoroutine(WaitTime());
        _mineTimer = gameObject.AddComponent<TimerForCircle>();
        _mineTimer.InitTimerForCircle(_speed, new Dictionary<double, Action> { { 0, TimerShot } }, circleAndText, CircleText);
        _timerForFastShot = gameObject.AddComponent<TimerForCircle>();
        _timerForFastShot.InitTimerForCircle(_speedFastShot, FastShot);
    }


    public void CircleText(double t)
    {
        circleAndText.SetTime(t, true);
    }

    private int _level;
    public void SetCannonLevel(int lvl)
    {
        _level = lvl;
        _lvlText.text = _level.ToString();
    }
    

    public void FastShot()
    {
       
    }

    public void TimerShot()
    {
        
    }

    public void BtnClickShot()
    {
        //��������� ����� � �������� ������ �����+1
    }
    public void CanonRotation(Vector3 target)
    {
        _canon.transform.DOLookAt(target,0.15f);
        Instantiate(_boom,_forParticleCreate);
        Instantiate(_puff,_forParticleCreate);
    }
    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(1f);
        _uiBottomPanel.ClickOnCenterButton();
    }

}
