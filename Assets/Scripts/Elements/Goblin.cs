using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : MonoBehaviour
{
    [SerializeField] private DestroyableObject _target;
    [SerializeField] private GameObject _3dModel;
    [SerializeField] private TMPro.TMP_Text _lvlText;
    [SerializeField] public ParticleSystem _particlesMerge;

    private int _level=0;
    private Vector2Int pos;
    private double _power;
    void Start()
    {
        pos = new Vector2Int((int)(transform.position.x), (int)(transform.position.z));
        EntryPoint.Instance.GoblinsManager.AddGoblinFromScene(this, pos);
        if (_level == 0) _level = 2; //2-th level for tests
        SetLevel(_level);
    }
    public void SetLevel(int lvl)
    {
        _level = lvl;
        _power = Balance.GoblinGetPowerByLevel(_level);
        _lvlText.text = _level.ToString();
    }


    public void MakeWork()
    {
        if (_target!=null) 
        _target.ConsumeDurability(_power);
    }

    public void OnStartMove()
    {
        EntryPoint.Instance.GoblinsManager.RemoveGoblinFromScene(new Vector2Int((int)(transform.position.x), (int)(transform.position.z)));

        if (_target != null)
            _target.GoblinNotWorkThereMore(this);
    }

    public void RemoveFromSceneOnPosition(Vector3 position)
    {
        EntryPoint.Instance.GoblinsManager.RemoveGoblinFromScene(new Vector2Int((int)position.x, (int)position.z));

        if (_target != null)
            _target.GoblinNotWorkThereMore(this);
    }

    public void OnChangePos(Vector3 position)
    {
        pos = new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.z));

        EntryPoint.Instance.GoblinsManager.AddGoblinFromScene(this, pos);
        TryFindNewTarget();
    }

    public bool IsCanMerge(Goblin other)
    {
        return other._level == _level;
    }

    public void Merge()
    {
        GoblinAudioManager.StopSoundMergeTarget();
        GoblinAudioManager.PlaySoundMergeSuccess();
        _level++;
        SetLevel(_level);
        Debug.Log($"{gameObject.name} now level {_level}!");
    }
    private void OnDestroy()
    {
        RemoveFromSceneOnPosition(transform.position);
        EntryPoint.Instance.GoblinsManager.GoblinsWasRemoved();
        EntryPoint.Instance.GoblinsManager.RemoveGoblinFromScene(pos);
    }
    public void OnWasMerged()
    {
        var fvx = Instantiate(_particlesMerge,transform.position + new Vector3(0f,0f,-0.5f), Quaternion.identity);
        fvx.transform.eulerAngles = new Vector3(-90f, 0f, 0f);

        Destroy(gameObject);
    }

    public void TryFindNewTarget(bool forceTarget = false, Vector2Int posTarget = default(Vector2Int))
    {
        _target = null;
        Vector2Int posGoblin = new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z));
        int posX = Mathf.RoundToInt(transform.position.x);
        int posY = Mathf.RoundToInt(transform.position.z);

        EntryPoint.Instance.LevelsManager.CurrentLevel.getPickableObjectByPos(posGoblin)?.PickUpThisObject();

        if (!forceTarget)
        {
            _target = EntryPoint.Instance.LevelsManager.CurrentLevel.getDestroyableObjectByPos(new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z+1)));
            if (_target == null)
                _target = EntryPoint.Instance.LevelsManager.CurrentLevel.getDestroyableObjectByPos(new Vector2Int(Mathf.RoundToInt(transform.position.x+1), Mathf.RoundToInt(transform.position.z)));
            if (_target == null)
                _target = EntryPoint.Instance.LevelsManager.CurrentLevel.getDestroyableObjectByPos(new Vector2Int(Mathf.RoundToInt(transform.position.x-1), Mathf.RoundToInt(transform.position.z)));
            if (_target == null)
                _target = EntryPoint.Instance.LevelsManager.CurrentLevel.getDestroyableObjectByPos(new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z-1)));
        }
        else
        {
            _target = EntryPoint.Instance.LevelsManager.CurrentLevel.getDestroyableObjectByPos(posTarget);
        }

        if (_target != null)
        {
            _target.GoblinStartWorkThere(this);
            _3dModel.transform.rotation = Quaternion.LookRotation((_target.GetTargetForGoblins() - _3dModel.transform.position).normalized);
        }
        else
            _3dModel.transform.rotation = Quaternion.identity;
    }

}
