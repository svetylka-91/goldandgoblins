using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forge : MonoBehaviour
{
    [SerializeField] private GameObject _circleProgressBar;
    [SerializeField] private CircleAndText circleAndText;
    [SerializeField] private TMPro.TMP_Text _moneyText;
    [SerializeField] private ButtonForUpgrade _buttonForUpgrade;

    private TimerForCircle _mineTimer;

    private int _level = 1;
    private float _speed = 2f;

    private void Awake()
    {
        _speed = Balance.ForgeSpeed;
        RecalculateMoneyIncome();

        _buttonForUpgrade.SetParams(
            Balance.ForgeBasePrice, 
            Balance.ForgeMyltiplyPricePerLevel, 
            Balance.ForgeBorderValues);
        _buttonForUpgrade.SetLevel(_level);
        _buttonForUpgrade.SetCalback(OnSetNewLevel);

        _mineTimer = gameObject.AddComponent<TimerForCircle>();
        _mineTimer.InitTimerForCircle(_speed, new Dictionary<double, Action> { { 0, ()=> { EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(Balance.ForgeMoneyIncomePerTime); } }}, circleAndText);
    }

    private void OnSetNewLevel(int level)
    {
        _level = level;
        RecalculateMoneyIncome();
    }

    public void RecalculateMoneyIncome()
    {
        _moneyText.text = SpecialFunctions.GetStringFromBigDouble(Balance.ForgeGetIncome(_level));
    }

}
