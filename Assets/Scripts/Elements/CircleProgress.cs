using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleProgress
{
    private bool _fillRadial = false;
    private Image _radialFill;

    public CircleProgress(Image radialFillPref, bool fillRadial = false)
    {
        _radialFill = radialFillPref;
        _fillRadial = fillRadial;
    }

    public void SetRadial(float amount)
    {
        if (!_fillRadial)
        {
            _radialFill.fillAmount = amount;
        }
        else
        {
            _radialFill.fillAmount = 1 - amount;
        }
    }
}
