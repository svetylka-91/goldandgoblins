using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class DestroyableObjectEditor : MonoBehaviour
{
    [Header("Durability Level")]
    [SerializeField] private int _level;
    [SerializeField] private DestroyableObject _destroyableObject;

    private void Update()
    {
        _destroyableObject.Level = _level;
    }
}
