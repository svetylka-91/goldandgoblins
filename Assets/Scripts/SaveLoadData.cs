using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadData
{
    

    //Market
    private bool _adsOffBuyed = false;
    public bool Gems160AlreadyUsed = false;
    public bool Gems500AlreadyUsed = false;
    public bool Gems1200AlreadyUsed = false;


    public AllCards allCards
    {
        get { return EntryPoint.Instance.CardsManager.Cards; }
        set
        {
            EntryPoint.Instance.CardsManager.Cards = value;
        }
    }

    public Dictionary<Gems, MineSideData> AllMineSideData
    {
        get { return EntryPoint.Instance.MineSidesManager.AllMineSideData; }
        set { 
            EntryPoint.Instance.MineSidesManager.AllMineSideData = value; 
        }
    }


    public bool AdsOffBuyed
    { 
        set { _adsOffBuyed = value; if (_adsOffBuyed == true) EntryPoint.Instance.UIManager.MarketUI.OnAdsOffBuyed?.Invoke(); } 
        get { return _adsOffBuyed; } 
    }

    //Currency
    public double Coins { 
        get => EntryPoint.Instance.CurrencyManager.Coins.Count; 
        set => EntryPoint.Instance.CurrencyManager.Coins.OnLoadCount = value; 
    }    
    public double Bottles { 
        get => EntryPoint.Instance.CurrencyManager.Bottles.Count; 
        set => EntryPoint.Instance.CurrencyManager.Bottles.OnLoadCount = value; 
    } 
    public double Gems { 
        get => EntryPoint.Instance.CurrencyManager.Gems.Count; 
        set => EntryPoint.Instance.CurrencyManager.Gems.OnLoadCount = value; 
    }

    public void ApplyLoadData()
    {
        EntryPoint.Instance.MineSidesManager.OnLoadData();
        EntryPoint.Instance.ChestsManager.OnLoadData();
    }


}
