using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerFlightElixirsAndGems : MonoBehaviour
{
    [SerializeField] private GameObject _objectShutdown;
    [SerializeField] private FlightResource[] _flightElixirsAndGems;
    [SerializeField] private AnimatorFlightResources _animatorFlightElixirsAndGems;
    [SerializeField] private bool _isElixirs;

    private float _delayTime = 0;
    private int _counterResources;
    private void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();

        AssignmentCounterResources();

        float _plusDelayTime = 0.5f / _counterResources;

        for (int i = 0; i < _counterResources; i++)
        {
            float _randomNumberX = Random.Range(-15f, 15f);
            float _randomNumberY = Random.Range(-15f, 15f);

            _flightElixirsAndGems[i].transform.position = new Vector2
                (
                _flightElixirsAndGems[i].transform.position.x + _randomNumberX,
                _flightElixirsAndGems[i].transform.position.y + _randomNumberY
                );

            _flightElixirsAndGems[i].gameObject.SetActive(true);
            _flightElixirsAndGems[i].InitAndSetDelayTime(_delayTime, rectTransform.anchoredPosition);
            _delayTime += _plusDelayTime;
        }
        _objectShutdown.SetActive(true);

    }

    private void AssignmentCounterResources()
    {
        if (_isElixirs)
            AssignmentCounterElixirs();
        else
            AssignmentCounterGems();

    }

    private void AssignmentCounterElixirs()
    {
        if (_animatorFlightElixirsAndGems.GetQuantityElixirs() > _flightElixirsAndGems.Length)
            _counterResources = _flightElixirsAndGems.Length;
        else
            _counterResources = _animatorFlightElixirsAndGems.GetQuantityElixirs();
    }

    private void AssignmentCounterGems()
    {
        if (_animatorFlightElixirsAndGems.GetQuantityGems() > _flightElixirsAndGems.Length)
            _counterResources = _flightElixirsAndGems.Length;
        else
            _counterResources = _animatorFlightElixirsAndGems.GetQuantityGems();
    }

}
