using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanelClckedOnCard : MonoBehaviour
{
    [SerializeField] private OneCardUI _cardUI;
    [SerializeField] private UIRareLine _bottomRareLine;

    [SerializeField] private TextMeshProUGUI _textTop;
    [SerializeField] private TextMeshProUGUI _textLevel;
    [SerializeField] private TextMeshProUGUI _textDescroption;

    [Header("Effect blocks")]
    [SerializeField] private UIEffectCardBlock _uiEffectCardBlock;


    [Header("Button blocks")]
    [SerializeField] private GameObject _blockToUpgrade;
    [SerializeField] private TextMeshProUGUI _textCountCards;
    [SerializeField] private TextMeshProUGUI _textCountBottles;
    [SerializeField] private Button _buttonUpgrade;

    [SerializeField] private GameObject _blockToMarket;
    [SerializeField] private Button _buttonMarket;

    private OneCard _cardData;
    private int _countNeedBotles;
    private int _countHaveCards;
    private int _countNeedCards;
    private bool _readyToUpgrade = false;

    private void Awake()
    {
        _buttonUpgrade.onClick.AddListener(OnClickUpgrade);
        _buttonMarket.onClick.AddListener(() =>
        {
            EntryPoint.Instance.UIManager.MarketUI.ShowOnChests();
            gameObject.SetActive(false);
        });
    }
    private void OnDestroy()
    {
        _buttonUpgrade.onClick.RemoveAllListeners();
        _buttonMarket.onClick.RemoveAllListeners();
    }

    public void GenerateScreen(int indexCard)
    {
        gameObject.SetActive(true);
        _cardData = EntryPoint.Instance.CardsManager.Cards.AllCardsData[indexCard];
        _cardUI.InitCardByIndex(indexCard);
        _bottomRareLine.GenerateLine(_cardUI);

        _textTop.text = _cardData.LongCaption;
        _textLevel.text = _cardData.Level > 0 ? _cardData.GetTextLevel() : $"�� ��������";
        _textDescroption.text = _cardData.GetDiscription();

        _uiEffectCardBlock.RegenByCard(_cardData);

        _readyToUpgrade = true;

        if (_cardData.Level > 0)
        {
            _textLevel.text = _cardData.GetTextLevel();
            _blockToMarket.SetActive(false);
            _blockToUpgrade.SetActive(true);

            _countHaveCards = _cardData.CountHaveCards;
            _countNeedCards = _cardData.GetCountNeedCards();
            _textCountCards.text = $"{_countHaveCards}/{_countNeedCards}";
            if (_countHaveCards >= _countNeedCards)
                _textCountCards.color = Color.white;
            else
            {
                _textCountCards.color = Color.red;
                _readyToUpgrade = false;
            }


            _countNeedBotles = _cardData.GetCountNeedBottles();
            _textCountBottles.text = _countNeedBotles.ToString();
            if (EntryPoint.Instance.CurrencyManager.Bottles.Count >= _countNeedBotles)
                _textCountBottles.color = Color.white;
            else
            {
                _textCountBottles.color = Color.red;
                _readyToUpgrade = false;
            }
        }
        else
        {
            _textLevel.text = $"�� ��������";
            _blockToMarket.SetActive(true);
            _blockToUpgrade.SetActive(false);
        }
    }

    public void OnClickUpgrade()
    {
        if (_readyToUpgrade)
        {
            gameObject.SetActive(false);
            EntryPoint.Instance.UIManager.GetUiUpgradeCardScreen().GenerateScreen(_cardData);
        }
        else
        {
            //TODO ���� ������� �����.
        }
    }
}
