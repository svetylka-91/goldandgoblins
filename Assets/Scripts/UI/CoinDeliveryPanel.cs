using UnityEngine;

public class CoinDeliveryPanel : MonoBehaviour
{
    [Header("Game object")]
    [SerializeField] private GameObject _panelCoinDelivery;

    private void Awake()
    {
        _panelCoinDelivery.SetActive(false);
    }

    public void OnClickButton()
    {
        _panelCoinDelivery.SetActive(false);
    }

    public void OnClickCoinDelivery()
    {
        _panelCoinDelivery.SetActive(true);
    }
}
