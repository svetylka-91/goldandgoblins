using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MarketGemCard : MonoBehaviour
{
    [Header("Count and price block")]
    [SerializeField] private GameObject _redLine;
    [SerializeField] private TextMeshProUGUI _greenText;
    [SerializeField] private TextMeshProUGUI _whiteText;
    [SerializeField] private TextMeshProUGUI _priceText;


    [Header("center block")]
    [SerializeField] private GameObject _x2Bonus;
    [SerializeField] private Image _image;

    private Action _onClick;
    private Button _button;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => _onClick?.Invoke());
    }
    private void OnDestroy()
    {
        _button.onClick.RemoveAllListeners();
    }

    public void Init(Sprite image, string price, int countWhite, bool alreadyUsed, Action onClick)
    {
        if (!alreadyUsed)
        {
            _redLine.gameObject.SetActive(true);
            _greenText.gameObject.SetActive(true);
            _greenText.text = (countWhite * 2).ToString();
            _x2Bonus.SetActive(true);
        }
        else
        {
            _redLine.gameObject.SetActive(false);
            _greenText.gameObject.SetActive(false);
            _x2Bonus.SetActive(false);
        }

        _whiteText.text = countWhite.ToString();
        _priceText.text = price.ToString();

        _image.sprite = image;

        _onClick = onClick;

    }



}
