using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotEnouthGems : MonoBehaviour
{
    [SerializeField] private Button _btnClose;
    [SerializeField] private Button _btnBuy;

    private void Awake()
    {
        _btnClose.onClick.AddListener(() => gameObject.SetActive(false));
        _btnBuy.onClick.AddListener(() => 
        { 
            gameObject.SetActive(false); EntryPoint.Instance.UIManager.MarketUI.ShowOnGems();
        });
    }

    private void OnDestroy()
    {
        _btnClose.onClick.RemoveAllListeners();
        _btnBuy.onClick.RemoveAllListeners();
    }


}
