using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketBuyGemsPanel : MonoBehaviour
{
    [SerializeField] private List<Sprite> _listImages;

    [SerializeField] private MarketGemCard[] _marketGemCards;

    public void Init()
    {
        InitCard1();
        InitCard2();
        InitCard3();
    }

    public void InitCard1() => _marketGemCards[0].Init(_listImages[0], "149,00�", 160, EntryPoint.Instance.SaveLoadManager.SLData.Gems160AlreadyUsed, ClickCard1);
    public void InitCard2() => _marketGemCards[1].Init(_listImages[1], "399,00�", 500, EntryPoint.Instance.SaveLoadManager.SLData.Gems500AlreadyUsed, ClickCard2);
    public void InitCard3() => _marketGemCards[2].Init(_listImages[2], "799,00�", 1200, EntryPoint.Instance.SaveLoadManager.SLData.Gems1200AlreadyUsed, ClickCard3);
    

    public void ClickCard1()
    {
        Add160Gems();
        EntryPoint.Instance.UIManager.MarketUI.ShowMoneyWasSpended();
    }
    public void ClickCard2()
    {
        Add500Gems();
        EntryPoint.Instance.UIManager.MarketUI.ShowMoneyWasSpended();
    }
    public void ClickCard3()
    {
        Add1200Gems();
        EntryPoint.Instance.UIManager.MarketUI.ShowMoneyWasSpended();
    }

    
    public void Add160Gems()
    {
        if (!EntryPoint.Instance.SaveLoadManager.SLData.Gems160AlreadyUsed)
            EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(160);

        EntryPoint.Instance.SaveLoadManager.SLData.Gems160AlreadyUsed = true;
        EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(160);
        InitCard1();
    }
    public void Add500Gems()
    {
        if (!EntryPoint.Instance.SaveLoadManager.SLData.Gems500AlreadyUsed)
            EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(500);

        EntryPoint.Instance.SaveLoadManager.SLData.Gems500AlreadyUsed = true;
        EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(500);
        InitCard2();
    }
    public void Add1200Gems()
    {
        if (!EntryPoint.Instance.SaveLoadManager.SLData.Gems1200AlreadyUsed)
            EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(1200);

        EntryPoint.Instance.SaveLoadManager.SLData.Gems1200AlreadyUsed = true;
        EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(1200);
        InitCard3();
    }
}
