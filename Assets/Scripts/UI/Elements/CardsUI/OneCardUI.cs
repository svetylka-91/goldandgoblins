﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using DG.Tweening;

public class OneCardUI : MonoBehaviour
{
    public int Index;
    public OneCard Card;

    [SerializeField] private Image _imageBackground;
    [SerializeField] private List<Sprite> _listImageBackground;

    [SerializeField] private TMP_Text _captionText;

    [Header("Center part")]
    [SerializeField] private Image _mainImageCard;
    [SerializeField] private TMP_Text _textTypeCard;
    [SerializeField] private Image _imageTypeCard;
    [SerializeField] private List<Sprite> _listImageTypeCard;


    [Header("Bottom part")]
    [SerializeField] private GameObject _bottomBlockOpen;
    [SerializeField] private GameObject _bottomBlockNotFinded;
    [SerializeField] private GameObject _bottomBlockClosed;

    [SerializeField] private GameObject _greenArrow;
    [SerializeField] private RectTransform _rtGreenArrow;
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private TMP_Text _levelProgressText;
    [SerializeField] private Image _imageFillProgress;

    [SerializeField] private TMP_Text _levelNeedToOpen;

    [Header("X part")]
    [SerializeField] private GameObject _bonusX;
    [SerializeField] private TextMeshProUGUI _bonusXText;

    [Header("This button")]
    [SerializeField] private Button _cardButtonComponent;
    [SerializeField] private GameObject _darkCardButton;

    private bool _posibleClick = true;
    private int _needCards = 0;

    private void Awake()
    {
        _cardButtonComponent.onClick.AddListener(ClickOnCard);
    }
    private void OnDestroy()
    {
        _cardButtonComponent.onClick.RemoveAllListeners();
        _bonusX.transform.DOKill();
    }

    public void SetPosibleClick(bool value) => _posibleClick = value;

    public void ClickOnCard()
    {
        if (!_posibleClick) return;
        UIAudioManager.PlaySoundCardOpen();
        EntryPoint.Instance.UIManager.ClickedOnCard(Index);
    }

    private void OnDisable()
    {
        _rtGreenArrow.DOKill();
    }

    public void RegenCard()
    {
        if (Card.RoundForOpen > EntryPoint.Instance.LevelsManager.NomerLevel)
        {
            _bottomBlockOpen.SetActive(false);
            _bottomBlockNotFinded.SetActive(false);
            _bottomBlockClosed.SetActive(true);
            _levelNeedToOpen.text = Card.GetTextRoundNeedForOpen();
            _darkCardButton.SetActive(true);
        }
        else if (Card.CountHaveCards == 0)
        {
            _bottomBlockOpen.SetActive(false);
            _bottomBlockNotFinded.SetActive(true);
            _bottomBlockClosed.SetActive(false);
            _darkCardButton.SetActive(false);
        }
        else
        {
            _bottomBlockOpen.SetActive(true);
            _bottomBlockNotFinded.SetActive(false);
            _bottomBlockClosed.SetActive(false);
            _darkCardButton.SetActive(false);
            _levelText.text = Card.GetTextLevel();
            _needCards = Card.GetCountNeedCards();
            _levelProgressText.text = Card.CountHaveCards.ToString() + "/" + _needCards.ToString();

            if (Card.CountHaveCards >= _needCards)
            {
                _imageFillProgress.color = new Color(64f / 256f, 219f / 256f, 0);
                _imageFillProgress.fillAmount = 1;
                _greenArrow.SetActive(true);

                if (Card.IsPosibleLevelUpNow())
                    MakeGreenArrowAnimation();
                else
                    OffGreenArrowAnimation();
            }
            else
            {
                _imageFillProgress.color = new Color(161f / 256f, 0, 188f / 256f);
                float fillAmount = ((float)Card.CountHaveCards / (float)_needCards);
                if (fillAmount > 1) fillAmount = 1;
                _imageFillProgress.fillAmount = fillAmount;
                _greenArrow.SetActive(false);
            }
        }
    }

    private void MakeGreenArrowAnimation()
    { 
        _rtGreenArrow.DOAnchorPosY(2, 0.3f).SetLoops(-1, LoopType.Yoyo);
    }
    private void OffGreenArrowAnimation()
    {
        _rtGreenArrow.DOKill();
        _rtGreenArrow.anchoredPosition = new Vector2(3.9f, -9f);
    }

    public void InitCardByIndex(int index)
    {
        _bonusX.SetActive(false);
        Index = index;
        Card = EntryPoint.Instance.CardsManager.Cards.AllCardsData[index];

        var _texture = Resources.Load(Card.GetFileNameImage(), typeof(Texture2D)) as Texture2D;
        _mainImageCard.sprite = Sprite.Create(_texture, new Rect(0, 0, _texture.width, _texture.height), new Vector2(0f, 0f));

        _imageBackground.sprite = _listImageBackground[(int)Card.Rare];

        _captionText.text = Card.GetShortCaptionText();
        _textTypeCard.text = Card.GetTypeText();

        _imageTypeCard.sprite = _listImageTypeCard[Card.GetIndexTypeImage()];

        RegenCard();
    }
    public void ShowBonusX(int bonus)
    {
        _bonusX.transform.localScale = Vector3.zero;
        _bonusX.SetActive(true);
        _bonusX.transform.DOScale(1, 0.4f);
        _bonusXText.text = bonus.ToString();
    }

}
