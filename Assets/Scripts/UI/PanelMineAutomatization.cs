﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanelMineAutomatization : MonoBehaviour
{
    [SerializeField] private OneCardUI _cardUI;

    [SerializeField] private TextMeshProUGUI _textTop;
    [SerializeField] private TextMeshProUGUI _textLevel;
    [SerializeField] private TextMeshProUGUI _textDescroption;

    [Header("Demands blocks")]
    [SerializeField] private TextMeshProUGUI _textLine1;
    [SerializeField] private GameObject _checkLine1;
    [SerializeField] private TextMeshProUGUI _textLine2;
    [SerializeField] private GameObject _checkLine2;


    [Header("Effect blocks")]
    [SerializeField] private UIEffectCardBlock _uiEffectCardBlock;

    [Header("Button blocks")]
    [SerializeField] private TextMeshProUGUI _textCountCards;
    [SerializeField] private TextMeshProUGUI _textCountBottles;
    [SerializeField] private Button _buttonUpgrade;
    [SerializeField] private Button _buttonAuto;

    private Gems _typeMine;
    private OneCard _cardData;
    private bool _posibleToAuto=false;
    private bool _readyToUpgrade = false;

    private void Awake()
    {
        _buttonAuto.onClick.AddListener(OnClickAutomatization);
        _buttonUpgrade.onClick.AddListener(OnClickUpgrade);
    }

    private void OnDestroy()
    {
        _buttonAuto.onClick.RemoveAllListeners();
        _buttonUpgrade.onClick.RemoveAllListeners();
    }

    public void GenerateScreen(Gems typeMine)
    {
        gameObject.SetActive(true);

        _typeMine = typeMine;
        DemandsCardForAuto demandsCardForAuto = EntryPoint.Instance.MineSidesManager.GetDemandsCardForAuto(typeMine);
        _cardData = demandsCardForAuto.Card;
        _cardUI.InitCardByIndex(_cardData.Index);
        _uiEffectCardBlock.RegenByCard(_cardData);

        _textTop.text = $"Управляющий ({_cardData.GetTypeText()})";

        _textLine1.text = EntryPoint.Instance.CardsManager.GetRareText(_cardData.Rare);
        if (_cardData.Level > 0)
        {
            _checkLine1.SetActive(true);
            _posibleToAuto = true;
        }
        else
        {
            _checkLine1.SetActive(false);
            _posibleToAuto = false;
        }


        _textLine2.text = $"Уровень {demandsCardForAuto.Level}";
        if (_cardData.Level >= demandsCardForAuto.Level)
            _checkLine2.SetActive(true);
        else
        {
            _checkLine2.SetActive(false);
            _posibleToAuto = false;
        }

        if (_posibleToAuto)
        {
            _buttonUpgrade.gameObject.SetActive(false);
            _buttonAuto.gameObject.SetActive(true);
            _uiEffectCardBlock.gameObject.SetActive(false);
        }
        else
        {
            _readyToUpgrade = true;

            var _countHaveCards = _cardData.CountHaveCards;
            var _countNeedCards = _cardData.GetCountNeedCards();
            _textCountCards.text = $"{_countHaveCards}/{_countNeedCards}";
            if (_countHaveCards >= _countNeedCards)
                _textCountCards.color = Color.white;
            else
            {
                _textCountCards.color = Color.red;
                _readyToUpgrade = false;
            }


            var _countNeedBotles = _cardData.GetCountNeedBottles();
            _textCountBottles.text = _countNeedBotles.ToString();
            if (EntryPoint.Instance.CurrencyManager.Bottles.Count >= _countNeedBotles)
                _textCountBottles.color = Color.white;
            else
            {
                _textCountBottles.color = Color.red;
                _readyToUpgrade = false;
            }

            _buttonUpgrade.gameObject.SetActive(true);
            _buttonAuto.gameObject.SetActive(false);
            _uiEffectCardBlock.gameObject.SetActive(true);
        }
    }


    private void OnClickAutomatization()
    {
        gameObject.SetActive(false);
        EntryPoint.Instance.MineSidesManager.SetAutomatization(_typeMine);
    }

    private void OnClickUpgrade()
    {
        if (_readyToUpgrade)
        {
            EntryPoint.Instance.UIManager.GetUiUpgradeCardScreen().GenerateScreen(_cardData);
            GenerateScreen(_typeMine);
        }
    }
}

