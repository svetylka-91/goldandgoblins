using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using DG.Tweening;

public class ButtonForUpgrade : MonoBehaviour
{
    [Header("X2 icon")]
    [SerializeField] private GameObject _x2GO;
    [SerializeField] private Image _x2Image;
    [SerializeField] private TextMeshProUGUI _x2Text;
    [SerializeField] RectTransform _x2GOTransform;

    [Header("Top line")]
    [SerializeField] private TextMeshProUGUI _levelText;
    [SerializeField] private Image _topGrey;
    [SerializeField] private Image _topGold;

    [Header("Down part")]
    [SerializeField] private Sprite _buttonGreen;
    [SerializeField] private Sprite _buttonGrey;


    [SerializeField] private TextMeshProUGUI _plusLevelText;
    [SerializeField] private GameObject _priceTextGO;
    [SerializeField] private Image _buttonGO;

    [SerializeField] private RectTransform _priceTextTransform;
    [SerializeField] private TextMeshProUGUI _priceText;

    private int _level=1;

    private double _money = 0;
    private double _startPrice = 0;
    private float _multyPricePerLVL = 0;
    private double _totalPrice = 0;

    private List<Balance.UpgradeBorderValue> _borderLevels;
    private int _resultLevelAfterUpgrade = 0;
    private bool _showX2WhenCLick = false;
    private int _nextX2Value = 2;

    private Action<int> _callback;

    private void Awake()
    {
        _levelText.text = _level.ToString();
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount += OnChangeMoney;
    }
    private void OnDestroy()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount -= OnChangeMoney;
    }

    public void SetLevel(int level) => _level = level;
    public void SetCalback(Action<int> action) => _callback = action;
    public void SetParams(double startPrice, float multyPricePerLVL, List<Balance.UpgradeBorderValue> borderLevels)
    {
        _level = 1;
        _multyPricePerLVL = multyPricePerLVL;
        _borderLevels = borderLevels;
        _startPrice = startPrice/ _multyPricePerLVL;
        CalculateButtonAllValues();
    }

    public void click()
    {
        
            EffectsAudioManager.PlaySoundUpLevel();
        _callback(_resultLevelAfterUpgrade);
        _level = _resultLevelAfterUpgrade;

        if (_showX2WhenCLick)
        {
            _x2GO.SetActive(true);
            _x2GOTransform.DOKill();
            _x2Text.DOKill();
            _x2Image.DOKill();

            _showX2WhenCLick = false;
            _x2GO.SetActive(true);
            _x2Image.color = new Color(44/255f, 172 / 255f, 229 / 255f, 1);
            _x2Text.color = new Color(1, 1, 1, 1);
            _x2Text.text = "x" + _nextX2Value.ToString();
            _x2GOTransform.anchoredPosition= new Vector3(0, 50, 0);

            _x2GOTransform.DOAnchorPosY(150, 2);
            _x2Text.DOFade(0, 1).SetDelay(1).OnComplete(() => { _x2GO.SetActive(false); });
            _x2Image.DOFade(0, 1).SetDelay(1);



            //TODO ��������� ��� ������� ��� ������� �� ��������. �����, ��� �� ���������� ����� ����� ������ ������ ������ +-20%
        }


        EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(-_totalPrice);
    }

    private void OnChangeMoney(double newMoneyValue)
    {
        _money = newMoneyValue;

        if (_startPrice>0) //����� �����, ��������� � ���, ��� � ��������� ���� ��������� ������, ����� ������ �� ���� ������
        CalculateButtonAllValues();
    }

    private void CalculateButtonAllValues()
    {
        //finding next borderlevel
        int borderLevel = 0;
        int prewBorderLevel = 0;
        for (int i = 0; i < _borderLevels.Count-1; i++)
        {
            if (_level < _borderLevels[i].Level)
            {
                borderLevel = _borderLevels[i].Level;
                _nextX2Value = _borderLevels[i].Myltiply;
                if (i > 0)
                    prewBorderLevel = _borderLevels[i - 1].Level;
                break;
            }
        }

        //not writed leveles
        if (borderLevel==0)
        {
            _nextX2Value = 2;
            borderLevel = (_level / 10 + 1) * 10;
            prewBorderLevel = (_level / 10) * 10;
        }


        double spendedMoney = _startPrice*((1 - Math.Pow(_multyPricePerLVL, _level)) / (1 - _multyPricePerLVL));
        int maxLevel = (int)(Math.Log(1+((spendedMoney + _money)/ _startPrice) * (_multyPricePerLVL-1), _multyPricePerLVL));
        if (maxLevel >= borderLevel)
        {
            _showX2WhenCLick = true;
            maxLevel = borderLevel;
        }
        else
            _showX2WhenCLick = false;

        _resultLevelAfterUpgrade = maxLevel;
        int additionalLevelAfterUpgrade = maxLevel - _level;
        double totalPriceMaxLevel = _startPrice *  ((1 - Math.Pow(_multyPricePerLVL, maxLevel)) / (1 - _multyPricePerLVL));
        _totalPrice = Math.Floor(totalPriceMaxLevel - spendedMoney);

        if (additionalLevelAfterUpgrade == 0)
        {
            _buttonGO.sprite = _buttonGrey;
            _plusLevelText.text = "+1";
            _priceText.text = SpecialFunctions.GetStringFromBigDouble(Math.Floor(_startPrice * Math.Pow(_multyPricePerLVL, _level)));
        }
        else
        {
            _buttonGO.sprite = _buttonGreen;
            _plusLevelText.text = "+" + additionalLevelAfterUpgrade.ToString();
            _priceText.text = SpecialFunctions.GetStringFromBigDouble(_totalPrice);
        }
        _priceTextTransform.anchoredPosition = new Vector3(40 + (170 - _priceText.preferredWidth) / 2, 30, 0);

        if (_level > 1)
        {
            _topGold.fillAmount = (float)(_level - prewBorderLevel) / (float)(borderLevel - prewBorderLevel);
            _topGrey.fillAmount = (float)(_level + additionalLevelAfterUpgrade - prewBorderLevel) / (float)(borderLevel - prewBorderLevel);
        }
        else
        {
            //special conditions for first level
            _topGold.fillAmount = 0;
            _topGrey.fillAmount = (float)(additionalLevelAfterUpgrade) / (float)(borderLevel - prewBorderLevel-1);
        }

        _levelText.text = _level.ToString();
    }
}