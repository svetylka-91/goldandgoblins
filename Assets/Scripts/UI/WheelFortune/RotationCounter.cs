using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class RotationCounter : MonoBehaviour
{
    [FormerlySerializedAs("_time")]
    [Header("Amount Of Rotation")]
    [SerializeField] private TMP_Text _textAmount;
    [SerializeField] private int _clickAmount;

    [Header("Next Time Rotation")]
    [SerializeField] private float _timeStart;
    [SerializeField] private float _timeCorrent;
    [SerializeField] private TMP_Text _timerText;

    private bool timer = false;

    [Header("Game Object")]
    [SerializeField] private GameObject _buttonRotate;
    [SerializeField] private GameObject _buttonNotReady;
    [SerializeField] private GameObject _textTotalAmount;
    [SerializeField] private GameObject _textNotTotalAmount;
    [SerializeField] private GameObject _textNextTimeRotation;

    [Header("Rotation Panels")]
    [SerializeField] private GameObject _panelFreeDailySpin;
    [SerializeField] private GameObject _panelRotationForAds;

    // это нужно почистить !!!
    private void Awake()
    {
        _panelFreeDailySpin.SetActive(true);
        _panelRotationForAds.SetActive(false);
    }

    private void Start()
    {
        _timeCorrent = _timeStart;
        _timerText.text = _timeCorrent.ToString();
    }

    private void Update()
    {
        if (timer == true)
        {
            _timeCorrent -= Time.deltaTime;
            // _timerText.text = Mathf.Round(_timeCorrent).ToString() + "с";
            _timerText.text = TimeConv.FormatForWheel(_timeCorrent);
        }

        if (_timeCorrent < 0)
        {
            _timeCorrent = 0;
            if (_clickAmount > 0)
            {
                _buttonNotReady.SetActive(false);
                _buttonRotate.SetActive(true);
            }
            _timeCorrent = _timeStart;
            timer = !timer;
        }

        if (_clickAmount == 5)
        {
            _textTotalAmount.SetActive(true);
            _textNotTotalAmount.SetActive(false);
            _textNextTimeRotation.SetActive(false);
        }

        if (_clickAmount < 5)
        {
            _textTotalAmount.SetActive(false);
            _textNotTotalAmount.SetActive(true);
            _textNextTimeRotation.SetActive(true);
        }
    }

    public void ButtonTimer()
    {
        timer = !timer;
    }

    public void OnButtonFreeDaily()
    {
        _panelFreeDailySpin.SetActive(false);
        _panelRotationForAds.SetActive(true);
    }

    public void OnButtonClick()
    {
        _clickAmount--;
        _buttonRotate.SetActive(false);
        _buttonNotReady.SetActive(true);

        if (_clickAmount <= 0)
        {
            _clickAmount = 0;
            timer = false;
            _buttonRotate.SetActive(false);
            _buttonNotReady.SetActive(true);
        }
        
        _textAmount.text = _clickAmount.ToString() + "/5";
    }
}
