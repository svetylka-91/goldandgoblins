using System.Collections;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class WheelFortune : MonoBehaviour
{
    [SerializeField] private TMP_Text winningText;
    [SerializeField] private string[] _prizeText;
    
    private int numberOfTurns;
    private int whatWeWin;

    private float speed; // скорость вращения и затухания

    private bool canWeTurn; // можно ли вращать колесо
    
    private void Start()
    {
        canWeTurn = true;
    }

    public void OnButtonClick()
    {
        StartCoroutine(TurnTheWheel());
    }

    private IEnumerator TurnTheWheel()
    {
        canWeTurn = false;

        numberOfTurns = Random.Range(30, 50);
        
        speed = 0.01f; // чем больше значение тем медленние

        for (int i = 0; i < numberOfTurns; i++)
        {
            transform.Rotate(0, 0 , 22.5f);

            // если кол. оборотов > 0.5(пройдено больше 50%), то замдляем его
            if (i > Mathf.RoundToInt(numberOfTurns * 0.5f))
            {
                speed = 0.02f;
            }
            if (i > Mathf.RoundToInt(numberOfTurns * 0.7f))
            {
                speed = 0.07f;
            }
            if (i > Mathf.RoundToInt(numberOfTurns * 0.9f))
            {
                speed = 0.1f;
            }

            yield return new WaitForSeconds(speed);
        }

        // если угол не попадает на нужное деление, то мы его докручиваем
        // 45, потому что у нас 8 делений на колесе (360/8 = 45)
        if (Mathf.RoundToInt(transform.eulerAngles.z) % 45 != 0)
        {
            transform.Rotate(0, 0, 22.5f); // доп. один оборот
        }

        whatWeWin = Mathf.RoundToInt(transform.eulerAngles.z);
        
        switch (whatWeWin)
        {
            // угол деления (сектора)
            case 0:
                winningText.text = _prizeText[0];
                break;
            case 45:
                winningText.text = _prizeText[1];
                break;
            case 90:
                winningText.text = _prizeText[2];
                break;
            case 135:
                winningText.text = _prizeText[3];
                break;
            case 180:
                winningText.text = _prizeText[4];
                break;
            case 225:
                winningText.text = _prizeText[5];
                break;
            case 270:
                winningText.text = _prizeText[6];
                break;
            case 315:
                winningText.text = _prizeText[7];
                break;
        }

        canWeTurn = true;
    }
}
