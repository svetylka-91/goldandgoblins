using UnityEngine;

public class PanelWheelFortune : MonoBehaviour
{
    [SerializeField] private GameObject _panelWheelFortune;

    public void OnClickButtonOpen()
    {
        _panelWheelFortune.SetActive(true);
    }

    public void OnClickButtonClose()
    {
        _panelWheelFortune.SetActive(false);
    }
}
