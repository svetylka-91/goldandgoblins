using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class UIBottomPanel : MonoBehaviour
{
    [Header("Center btn")]
    [SerializeField] private Image _imageCenterBtn;
    [SerializeField] private List<Sprite> _listImageCenterBtn;
    [SerializeField] private TextMeshProUGUI _TextPrice;
    [SerializeField] private TextMeshProUGUI _TextLevelNewGoblins;
    [SerializeField] private Image _imageFillProgresLevel;

    [Header("ChestButton")]
    [SerializeField] private Button _chestButton;
    [SerializeField] private TextMeshProUGUI _chestButtonIconText;
    [SerializeField] private GameObject _chestButtonIcon;

    [Header("CardButton")]
    [SerializeField] private Button _cardsButton;
    [SerializeField] private TextMeshProUGUI _cardButtonIconText;
    [SerializeField] private GameObject _cardButtonIcon;

    [Header("Other")]
    [SerializeField] BarrelScript _barrel;

    private double _money;

    public void Init()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount += OnChangeMoney;
        EntryPoint.Instance.ChestsManager.OnChangeCountFreeChests += OnChangeCountFreeChests;
        EntryPoint.Instance.CardsManager.OnChangeCountCardsPosibleUp += OnChangeCountCardsPosibleUp;
        _chestButton.onClick.AddListener(OnClickChestButton);
        _cardsButton.onClick.AddListener(OnClickCardsButton);
        UpdateCenterBtn();
        //_freePos[0] = new Vector3(2, 0, 5);
    }

    private void OnDestroy()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount -= OnChangeMoney;
        EntryPoint.Instance.ChestsManager.OnChangeCountFreeChests -= OnChangeCountFreeChests;
        EntryPoint.Instance.CardsManager.OnChangeCountCardsPosibleUp -= OnChangeCountCardsPosibleUp;
        _chestButton.onClick.RemoveAllListeners();
        _cardsButton.onClick.RemoveAllListeners();
    }
    private void OnChangeMoney(double newMoneyValue)
    {
        _money = newMoneyValue;
        UpdateCenterBtn();
    }
    private void OnChangeCountFreeChests(int count)
    {
        if (count > 0)
        {
            _chestButtonIconText.text = $"{count}";
            _chestButtonIcon.SetActive(true);
        }
        else
            _chestButtonIcon.SetActive(false);
    }

    private void OnChangeCountCardsPosibleUp(int count)
    {
        if (count > 0)
        {
            _cardButtonIconText.text = $"{count}";
            _cardButtonIcon.SetActive(true);
        }
        else
            _cardButtonIcon.SetActive(false);
    }

    private void OnClickCardsButton() => EntryPoint.Instance.UIManager.ShowCardsPanel();
    private void OnClickChestButton() => EntryPoint.Instance.UIManager.ShowMarketPanel();

    private void UpdateCenterBtn()
    {
        int countBuyGoblins = EntryPoint.Instance.GoblinsManager.CountBuyGoblins;

        _TextLevelNewGoblins.text = "���� " + (1 + (int)(countBuyGoblins / 8)).ToString();
        _imageFillProgresLevel.fillAmount = (countBuyGoblins % 8) / 8f;

        _TextPrice.text = SpecialFunctions.GetStringFromBigDouble(EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin);

        if (EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin <= _money)
        {
            _imageCenterBtn.sprite = _listImageCenterBtn[0];
        }
        else
            _imageCenterBtn.sprite = _listImageCenterBtn[1];

    }
    public void ClickOnCenterButton()
    {
        if (_imageCenterBtn.sprite == _listImageCenterBtn[0])
            EffectsAudioManager.PlaySoundMortarShot();
        Fire();
    }
    public void Fire()
    {
        Vector3[] waypoints = new Vector3[5];
        Vector3[] freePos = new Vector3[14];
        var barrelCountLine = 6;
        var isFreePosition = false;

        for (int i = 0; i < freePos.Length; i++)
        {
            if (i <= barrelCountLine)
                freePos[i] = new Vector3(2 + i, 0, 5);
            if (i > barrelCountLine)
                freePos[i] = new Vector3(15 - i, 0, 6);

            isFreePosition = EntryPoint.Instance.LevelsManager.CurrentLevel.IsPlaceFreeByPos((int)freePos[i].x, (int)freePos[i].z);

            if (isFreePosition)
            {
                Vector3 createBarrelPos = freePos[i];

                if (EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin > _money)
                    return;
                if (!EntryPoint.Instance.GoblinsManager.IsHaveLimitForNewBarrel())
                    return;

                Cannon cannon = EntryPoint.Instance.LevelsManager.GetCannon();
                cannon.CanonRotation(createBarrelPos);

                int levelForNextGoblin = 1 + (int)(EntryPoint.Instance.GoblinsManager.CountBuyGoblins / 8);
                EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(-EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin);
                EntryPoint.Instance.GoblinsManager.CountBuyGoblins++;

                waypoints[0] = cannon.transform.position;
                waypoints[1] = new Vector3(createBarrelPos.x + ((cannon.transform.position.x - createBarrelPos.x) * 0.4f), cannon.transform.position.y + 1, createBarrelPos.z - ((createBarrelPos.z - cannon.transform.position.z) * 0.4f));
                waypoints[2] = new Vector3(createBarrelPos.x + ((cannon.transform.position.x - createBarrelPos.x) * 0.5f), cannon.transform.position.y + 2, createBarrelPos.z - ((createBarrelPos.z - cannon.transform.position.z) * 0.5f));
                waypoints[3] = new Vector3(createBarrelPos.x + ((cannon.transform.position.x - createBarrelPos.x) * 0.6f), cannon.transform.position.y + 1, createBarrelPos.z - ((createBarrelPos.z - cannon.transform.position.z) * 0.6f));
                waypoints[4] = createBarrelPos;

                BarrelScript barrel = Instantiate(_barrel, cannon.transform.position, Quaternion.identity);
                Tween move = barrel.gameObject.transform.DOPath(waypoints, 0.1f, PathType.CatmullRom)
                    .SetLookAt(0.001f)
                    .SetEase(Ease.Linear)
                    .OnComplete(() => BarrelFinishedMove(barrel, createBarrelPos, levelForNextGoblin));

                UpdateCenterBtn();
                break;
            }

        }
    }

    public void BarrelFinishedMove(BarrelScript barrel, Vector3 pos, int levelGoblin)
    {
        barrel.gameObject.transform.position = pos;
        barrel.gameObject.transform.rotation = Quaternion.identity;
        barrel.InitBarrel(levelGoblin);
    }
}
