using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MarketUI : MonoBehaviour
{
    [SerializeField] private GameObject _specialOffers;
    [SerializeField] private Button _diableAdsOffer;


    [Header("�hests")]
    [SerializeField] private Button _btnChest1;
    [SerializeField] private Button _btnChest2;
    [SerializeField] private Button _btnChest3;

    [SerializeField] private TextMeshProUGUI _Chest2Price;
    [SerializeField] private TextMeshProUGUI _Chest3Price;

    [SerializeField] private TextMeshProUGUI _Chest1text2;
    [SerializeField] private TextMeshProUGUI _Chest1textNext;
    [SerializeField] private TextMeshProUGUI _Chest1textTime;

    [SerializeField] private Image _chest1AdsImage;


    [Header("UI pnales")]
    [SerializeField] private UIBuyChest _buyChest;
    [SerializeField] private NotEnouthGems _notEnouthGems;
    [SerializeField] private GameObject _infoAboutSpendMoneys;
    [SerializeField] private GameObject _infoAboutSpendGemsForBottles;
    [SerializeField] public MarketBuyGemsPanel MarketBuyGemsPanel;

    [Header("ContainerView")]
    [SerializeField] private RectTransform _rtContainerView;
    [SerializeField] private RectTransform _rtChestsPanel;
    [SerializeField] private RectTransform _rtGemsPanel;
    [SerializeField] private RectTransform _rtBottlesPanel;

    public Action OnAdsOffBuyed;

    private List<ChestType> _chestTypes;
    public void Init()
    {
        _chestTypes = new List<ChestType>() { ChestType.Wood, ChestType.Iron, ChestType.Gold};

        _btnChest1.onClick.AddListener(ClickBtnChest1);
        _btnChest2.onClick.AddListener(ClickBtnChest2);
        _btnChest3.onClick.AddListener(ClickBtnChest3);
        _diableAdsOffer.onClick.AddListener(ClickDisableAdsOffer);
        OnAdsOffBuyed += OffAdsOffOffer;
        EntryPoint.Instance.ChestsManager.OnChangeCountFreeChests += OnChangeCountFreeChests;
        MarketBuyGemsPanel.Init();
    }

    private void OnDestroy()
    {
        _btnChest1.onClick.RemoveAllListeners();
        _btnChest2.onClick.RemoveAllListeners();
        _btnChest3.onClick.RemoveAllListeners();
        _diableAdsOffer.onClick.RemoveAllListeners();
        OnAdsOffBuyed -= OffAdsOffOffer;
        EntryPoint.Instance.ChestsManager.OnChangeCountFreeChests -= OnChangeCountFreeChests;
    }

    public void ShowBottlesWasBuyed() => _infoAboutSpendGemsForBottles.SetActive(true);
    public void ShowMoneyWasSpended() => _infoAboutSpendMoneys.SetActive(true);
    public void ShowNotEnouthGems() => _notEnouthGems.gameObject.SetActive(true); 

    private void OffAdsOffOffer()
    {
        _diableAdsOffer.gameObject.SetActive(false);
        //In future we can add more special offers and not make off for panel
        _specialOffers.SetActive(false);
    }
    private void ClickDisableAdsOffer()
    {
        EntryPoint.Instance.SaveLoadManager.SLData.AdsOffBuyed = true;
    }

    private void MakeOffChildrenUI()
    {
        _buyChest.gameObject.SetActive(false);
    }
    private void ClickBtnChest1()
    {
        if (EntryPoint.Instance.ChestsManager.CountFreeChests==0)
        {
            EntryPoint.Instance.UIManager.ShowAds(()=>_buyChest.GenerateChest(_chestTypes[0]));
        }
        else
        {
            _buyChest.GenerateChest(_chestTypes[0]);
            OnEnable();
        }
    }

    private void ClickBtnChest2() => _buyChest.GenerateChest(_chestTypes[1]);
    private void ClickBtnChest3() => _buyChest.GenerateChest(_chestTypes[2]);

    public void ShowOnGems()
    {
        EntryPoint.Instance.UIManager.ShowMarketPanel();
        MakeOffChildrenUI();
        _rtContainerView.anchoredPosition = new Vector2(0, _rtGemsPanel.anchoredPosition.y*-1);
    }
    public void ShowOnBottles()
    {
        EntryPoint.Instance.UIManager.ShowMarketPanel();
        MakeOffChildrenUI();
        _rtContainerView.anchoredPosition = new Vector2(0, _rtBottlesPanel.anchoredPosition.y*-1);
    }
    public void ShowOnChests()
    {
        EntryPoint.Instance.UIManager.ShowMarketPanel();
        MakeOffChildrenUI();
        _rtContainerView.anchoredPosition = new Vector2(0, _rtChestsPanel.anchoredPosition.y*-1);
    }

    private void OnChangeCountFreeChests(int count) => UpdateFreeChestUI();
    private void UpdateFreeChestUI()
    {
        if (EntryPoint.Instance.ChestsManager.CountFreeChests == 2)
        {
            _Chest1text2.gameObject.SetActive(true);
            _Chest1textNext.gameObject.SetActive(false);
            _Chest1textTime.gameObject.SetActive(false);
            _chest1AdsImage.gameObject.SetActive(false);
            return;
        }
        else
        {
            if (EntryPoint.Instance.ChestsManager.CountFreeChests == 0)
                _chest1AdsImage.gameObject.SetActive(true);
            else
                _chest1AdsImage.gameObject.SetActive(false);


            _Chest1text2.gameObject.SetActive(false);
            _Chest1textNext.gameObject.SetActive(true);
            _Chest1textTime.gameObject.SetActive(true);
            _Chest1textTime.text = EntryPoint.Instance.ChestsManager.TimeForNextFreeChest();
        }
    }

    private void OnEnable()
    {
        if (EntryPoint.Instance == null) return;

        _rtContainerView.anchoredPosition = new Vector2(0, 0);
        UpdateFreeChestUI();
    }
}
