using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MarketBottleCard : MonoBehaviour
{
    [Header("data this card")]
    [SerializeField] private int _price = 60;
    [SerializeField] private int _countBottles = 1000;
    [SerializeField] public AnimatorFlightResources _animatorFlightGold;


    private Button _button;
    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(onClick);
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveAllListeners();
    }

    private void onClick()
    {
        if (_price <= EntryPoint.Instance.CurrencyManager.Gems.Count)
        {
            EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(-_price);
            EntryPoint.Instance.CurrencyManager.Bottles.ChangeCount(_countBottles);

            _animatorFlightGold.SpawnElixirs(transform.position, (int)_countBottles);
            EntryPoint.Instance.UIManager.MarketUI.ShowBottlesWasBuyed();

        }
        else
        {
            EntryPoint.Instance.UIManager.MarketUI.ShowNotEnouthGems();
        }
    }
}
