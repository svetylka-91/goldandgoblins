using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMineChest : BaseUIChestWindow
{

    [SerializeField] private Button _freeBtn;

    public override void AwakeUI()
    {
        _freeBtn.onClick.AddListener(OnClick);
    }

    public override void OnDestroyUI()
    {
        _freeBtn.onClick.RemoveAllListeners();
    }

    private void OnClick()
    {
        gameObject.SetActive(false);
        TryOpenChest();
    }

    public override void GenerateChest(ChestType chestType)
    {
        int minBottles = EntryPoint.Instance.LevelsManager.NomerLevel*9;
        int maxBottles = EntryPoint.Instance.LevelsManager.NomerLevel*11;
        
        _typeChest = chestType;
        _chestUI.MakeScreen(_typeChest, minBottles, maxBottles);
        _countBottles = UnityEngine.Random.Range(minBottles, maxBottles);

        gameObject.SetActive(true);
    }

    public void SetTopText(string text)
    {
        _windowBackground.SwichToOneLine();
        _windowBackground.SetText("������");
    }
}
