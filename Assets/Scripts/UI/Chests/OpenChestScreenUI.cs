using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class OpenChestScreenUI : MonoBehaviour
{
    private enum State { Init, WaitClick, AnimationInProcess, ShowCards, Finish }


    [SerializeField] private GameObject _cardPrefab;

    [Header("BottleCard")]
    [SerializeField] private GameObject _bottleCard;
    [SerializeField] private TextMeshProUGUI _bottleCount;

    [Header("SceneElements")]
    [SerializeField] private RawImage _chestImage;
    [SerializeField] private TextMeshProUGUI _topText;
    [SerializeField] private TextMeshProUGUI _bottomText;

    [SerializeField] private GameObject _cardsCountObject;
    [SerializeField] private TextMeshProUGUI _cardsCountText;

    private RectTransform _rtChestImage;
    private ChestControler _currentChestController;

    private List<OneCardUI> _allCardsScripts;
    private List<RectTransform> _allCardsRT;
    private State _state;
    private int _currentShowReward = 0;

    private void Awake()
    {
        _rtChestImage = _chestImage.GetComponent<RectTransform>();

        _allCardsRT = new List<RectTransform>();
        _allCardsScripts = new List<OneCardUI>();

        _allCardsRT.Add(_bottleCard.GetComponent<RectTransform>());

        for (int i = 0; i < 5; i++)
        {
            _allCardsRT.Add(Instantiate(_cardPrefab, transform).GetComponent<RectTransform>());
            _allCardsScripts.Add(_allCardsRT[i + 1].GetComponent<OneCardUI>());
        }
    }

    private void OnDisable()
    {
        _currentChestController.CloseChest();
    }


    public void Init(ChestType type, int bottles)
    {
        EntryPoint.Instance.ChestsManager.GenerateRewards(type);
        EntryPoint.Instance.ChestsManager.AddCurentRewardsToCards();
        _bottleCount.text = "+" + bottles.ToString();
        EntryPoint.Instance.CurrencyManager.Bottles.ChangeCount(bottles);

        foreach (var item in _allCardsRT)
        {
            item.gameObject.SetActive(false);
        }

        _cardsCountText.text = (EntryPoint.Instance.ChestsManager.ListCardsRewards.Count + 1).ToString(); //Cards + bottles
        _currentShowReward = 0;

        _currentChestController = EntryPoint.Instance.ChestsManager.AllChests[type].ChestControler;
        SetState(State.Init);
        _rtChestImage.DOKill();

        _cardsCountObject.SetActive(false);
        _topText.gameObject.SetActive(true);
        _topText.text = EntryPoint.Instance.ChestsManager.AllChests[type].Caption;
        _bottomText.gameObject.SetActive(false);
        _rtChestImage.localScale = Vector3.one;
        _rtChestImage.anchoredPosition = new Vector3(0, 3000, 0);
        _chestImage.texture = EntryPoint.Instance.ChestsManager.AllChests[type].ClosedTexture;

        AnimationStage1();
    }

    private void SetState(State state)
    {
        _state = state;
    }
    private void AnimationStage1()
    {
        _rtChestImage.DOAnchorPosY(800, 0.5f).OnComplete(() => { _bottomText.gameObject.SetActive(true); SetState(State.WaitClick); });
    }
    private void AnimationOpening()
    {
        SetState(State.AnimationInProcess);
        _currentChestController.ShowParticleOpening();
        Invoke("ShowCards", 1f);
    }
    private void ShowCards()
    {
        SetState(State.ShowCards);
        _topText.gameObject.SetActive(false);

        _rtChestImage.DOScale(0.5f, 0.5f);
        _rtChestImage.DOAnchorPosY(50, 0.5f);
        _currentChestController.OpenChest(0.5f);
        _bottomText.gameObject.SetActive(false);

        _cardsCountObject.SetActive(true);

        ShowCard(0);
    }

    private void RemoveCard(int nomer)
    {        
        _allCardsRT[nomer].anchoredPosition = new Vector2(0, -800);
        _allCardsRT[nomer].DOAnchorPos(new Vector2(-800, -800), 0.5f).SetDelay(0.5f);

    }
    private void ShowCard(int nomer)
    {
        CallDOKillAllElements();
        _allCardsRT[nomer].gameObject.SetActive(true);        
        _allCardsRT[nomer].anchoredPosition = new Vector2(800, -800);
        _allCardsRT[nomer].DOAnchorPos(new Vector2(0, -800), 1f).OnComplete(() => SetState(State.ShowCards));

        _cardsCountText.text = (EntryPoint.Instance.ChestsManager.ListCardsRewards.Count + 1 - _currentShowReward).ToString();

        if (nomer > 0)
        {
            MakeCardContent(nomer - 1); //�� �� ������� ������ �������� ������ �� -1.
        }
    }

    private void MakeCardContent(int nomer)
    {
        _allCardsScripts[nomer].InitCardByIndex(EntryPoint.Instance.ChestsManager.ListCardsRewards[nomer].OneCard.Index);
        _allCardsScripts[nomer].SetPosibleClick(false);
        _allCardsScripts[nomer].ShowBonusX(EntryPoint.Instance.ChestsManager.ListCardsRewards[nomer].Count);
    }

    private void NextCard()
    {
        CallDOKillAllElements();
        _currentShowReward++;
        if (_currentShowReward > EntryPoint.Instance.ChestsManager.ListCardsRewards.Count)
        {
            ShowAllWhenFinish();
            return;
        }
        SetState(State.AnimationInProcess);
        ShowCard(_currentShowReward);
        RemoveCard(_currentShowReward - 1);
    }

    private void CallDOKillAllElements()
    {
        foreach (var item in _allCardsRT)
        {
            item.DOKill();
        }
    }
    private void ShowAllWhenFinish()
    {
        _cardsCountObject.SetActive(false);
        SetState(State.Finish);
        _cardsCountObject.SetActive(false);
        _topText.gameObject.SetActive(true);
        _topText.text = "�������� �������";


        _allCardsRT[0].anchoredPosition = new Vector2(-350, -400);
        _allCardsRT[1].anchoredPosition = new Vector2(0, -400);
        _allCardsRT[2].anchoredPosition = new Vector2(350, -400);
        _allCardsRT[3].anchoredPosition = new Vector2(-350, -850);
        _allCardsRT[4].anchoredPosition = new Vector2(0, -850);
        _allCardsRT[5].anchoredPosition = new Vector2(350, -850);
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (_state == State.AnimationInProcess) return;

            if (_state == State.WaitClick)
            {
                AnimationOpening();
            }
            else
            if (_state == State.ShowCards)
            {
                NextCard();
            }
            else
            if (_state == State.Finish)
            {
                gameObject.SetActive(false);
            }
        }
    }

}
