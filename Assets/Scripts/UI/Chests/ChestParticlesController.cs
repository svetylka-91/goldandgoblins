using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestParticlesController : MonoBehaviour
{

    [SerializeField] ParticleSystem _idleChestGlow;
    [SerializeField] ParticleSystem _idleChestBoom;
    [SerializeField] ParticleSystem _chestBoom;
    [SerializeField] ParticleSystem _chestGlow;
    [SerializeField] ParticleSystem _chestSphere;

    public void OffParticles()
    {
        _idleChestGlow.Stop();
        _idleChestBoom.Stop();
        _chestBoom.Stop();
        _chestGlow.Stop();
        _chestSphere.Stop();
    }

    public void StartIdle()
    {
        _idleChestGlow.Play();
        _idleChestBoom.Play();

        _idleChestBoom.gameObject.SetActive(true);
        _idleChestGlow.gameObject.SetActive(true);
    }

    public void StartOpening()
    {
        _chestBoom.Play();
        _chestGlow.Play();
        _chestSphere.Play();

        _chestBoom.gameObject.SetActive(true);
        _chestGlow.gameObject.SetActive(true);
        _chestSphere.gameObject.SetActive(true);
    }

}
