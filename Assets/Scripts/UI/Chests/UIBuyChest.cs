using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using System;

public class UIBuyChest : BaseUIChestWindow
{
    [Header("Free btn")]
    [SerializeField] private GameObject _goFreeBlock;
    [SerializeField] private Button _freeBtn;
    [SerializeField] private TextMeshProUGUI _freeBottomText;
    [SerializeField] private TextMeshProUGUI _freeBottomTextTime;

    [Header("Gems btn")]
    [SerializeField] private GameObject _goGemBlock;
    [SerializeField] private Button _gemBtn;
    [SerializeField] private TextMeshProUGUI _gemCountText;

    private int _price;

    public override void AwakeUI()
    {
        _freeBtn.onClick.AddListener(ClickGetChest);
        _gemBtn.onClick.AddListener(ClickGetChest);
    }
    public override void OnDestroyUI()
    {
        _freeBtn.onClick.RemoveAllListeners();
        _gemBtn.onClick.RemoveAllListeners();
    }

    private void ShowChestByPrice(int price)
    {
        _price = price;
        gameObject.SetActive(true);
        AnimateWindow();
        _windowBackground.SetText("������");
        _goFreeBlock.SetActive(false);
        _goGemBlock.SetActive(true);
        _gemCountText.text = price.ToString();

        if (price > EntryPoint.Instance.CurrencyManager.Gems.Count)
            _gemCountText.color = Color.red;
        else
            _gemCountText.color = Color.white;
    }
    private void ShowChestFree()
    {
        _price = 0;
        gameObject.SetActive(true);
        AnimateWindow();
        _windowBackground.SetText("���������");
        _goFreeBlock.SetActive(true);
        _goGemBlock.SetActive(false);
    }


    public override void GenerateChest(ChestType chestType)
    {
        _typeChest = chestType;

        switch (chestType)
        {
            case ChestType.Wood:
                GenerateWood();
                break;
            case ChestType.Iron:
                GenerateIron();
                break;
            case ChestType.Gold:
                GenerateGold();
                break;
            default:
                Debug.LogError("not implemented a chest");
                break;
        }
    }
    private void GenerateWood()
    {
        ShowChestFree();
        _chestUI.MakeScreen(_typeChest, 50, 61);
        _countBottles = UnityEngine.Random.Range(50, 61);

        if (EntryPoint.Instance.ChestsManager.CountFreeChests<2)
        {
            _freeBottomText.gameObject.SetActive(true);
            _freeBottomTextTime.gameObject.SetActive(true);
            _freeBottomTextTime.text = EntryPoint.Instance.ChestsManager.TimeForNextFreeChest();
        }
        else
        {
            _freeBottomText.gameObject.SetActive(false);
            _freeBottomTextTime.gameObject.SetActive(false);
        }
    }
    private void GenerateIron()
    {
        ShowChestByPrice(750);
        _chestUI.MakeScreen(_typeChest, 1007, 1113);
        _countBottles = UnityEngine.Random.Range(1007, 1113);
    }
    private void GenerateGold()
    {
        ShowChestByPrice(3500);
        _chestUI.MakeScreen(_typeChest, 4134, 4346);
        _countBottles = UnityEngine.Random.Range(4134, 4346);
    }

    private void ClickGetChest()
    {
        if (_price <= EntryPoint.Instance.CurrencyManager.Gems.Count)
        {
            if (_price == 0)
                EntryPoint.Instance.ChestsManager.AddFreeTimeForFullFreeChests();
            else
                EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(-_price);

            TryOpenChest();
        }
        else
        {
            EntryPoint.Instance.UIManager.MarketUI.ShowNotEnouthGems();
        }
    }

}
