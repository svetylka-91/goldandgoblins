using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChestUI : MonoBehaviour
{
    [SerializeField] private UIRareLine _rareLine;
    [SerializeField] private ChestImageByRendererTexture _chest;
    [SerializeField] private TextMeshProUGUI _bottleText;
    [SerializeField] private TextMeshProUGUI _cardsText;

    [Header("bottom panel")]
    [SerializeField] private GameObject _bottomBlockTotall;
    [SerializeField] private GameObject _bottomBlock1;
    [SerializeField] private GameObject _bottomBlock2;
    [SerializeField] private GameObject _bottomBlock3;
    [SerializeField] private TextMeshProUGUI _bottomBlockText1Count;
    [SerializeField] private TextMeshProUGUI _bottomBlockText2Count;
    [SerializeField] private TextMeshProUGUI _bottomBlockText3Count;


    public void MakeScreen(ChestType chest, int bottlemin, int bottlemax)
    {
        _rareLine.GenerateLineNoCards(EntryPoint.Instance.ChestsManager.AllChests[chest].Caption);
        _chest.SetChestTexure(EntryPoint.Instance.ChestsManager.AllChests[chest].ClosedTexture);

        _bottleText.text  = $"{bottlemin}-{bottlemax}";
        _cardsText.text = $"{EntryPoint.Instance.ChestsManager.AllChests[chest].TotalCountCards}";

        Dictionary<RareCard, int> countRareTypes = EntryPoint.Instance.ChestsManager.AllChests[chest].CountCards;

        if (countRareTypes.Count==1)
            _bottomBlockTotall.SetActive(false);
        else
            _bottomBlockTotall.SetActive(true); 
        
        if (countRareTypes.ContainsKey(RareCard.Uncommon))
        {
            _bottomBlock1.SetActive(true);
            _bottomBlockText1Count.text = $"x{countRareTypes[RareCard.Uncommon]}";
        }

        if (countRareTypes.ContainsKey(RareCard.Rare))
        {
            _bottomBlock2.SetActive(true);
            _bottomBlockText2Count.text = $"x{countRareTypes[RareCard.Rare]}";
        } 
        else _bottomBlock2.SetActive(false);
        
        if (countRareTypes.ContainsKey(RareCard.Unique))
        {
            _bottomBlock3.SetActive(true);
            _bottomBlockText3Count.text = $"x{countRareTypes[RareCard.Unique]}";
        } 
        else _bottomBlock3.SetActive(false);


    }

}
