using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUIChestWindow : MonoBehaviour
{
    [SerializeField] protected GameObject _windowGameObject;
    [SerializeField] protected UIWindowBackground _windowBackground;
    [SerializeField] protected ChestUI _chestUI;


    protected ChestType _typeChest;
    protected int _countBottles;

    private void Awake()
    {
        _windowBackground.GetBtnClose.onClick.AddListener(() => { gameObject.SetActive(false); });
        _windowBackground.SwichToOneLine();
        AwakeUI();
    }
    private void OnDestroy()
    {
        _windowBackground.GetBtnClose.onClick.RemoveAllListeners();
        OnDestroyUI();
    }
    protected void AnimateWindow()
    {
        _windowGameObject.transform.DOKill();
        _windowGameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        _windowGameObject.transform.DOScale(1, 0.5f);
    }

    protected void TryOpenChest()
    {
        gameObject.SetActive(false);
        EntryPoint.Instance.UIManager.ShowOpenChestScreenUI(_typeChest, _countBottles);
    }

    public abstract void AwakeUI();
    public abstract void OnDestroyUI();
    public abstract void GenerateChest(ChestType chestType);

}
