using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WelcomeBack : MonoBehaviour
    {
        [Header("Game object")]
        [SerializeField] private GameObject _panelWB;

        [Header("Button x2")]
        [SerializeField] private Image _imageButton;
        [SerializeField] private Sprite _spriteButtonDefault;
        [SerializeField] private Sprite _spriteButtonADS;

        [Header("Picture")]
        [SerializeField] private Image _imagePicture;
        [SerializeField] private Sprite _spriteUpdate;
        [SerializeField] private Sprite _spriteTV;

        [Header("Setting")]
        [SerializeField] private float _delayADS; // 3s default

        private void Awake()
        {
            _panelWB.SetActive(false);
        }

        private void Start()
        {
            ButtonDefault();
            _panelWB.SetActive(true);
            
            StartCoroutine(Delay());
        }

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(_delayADS);
            ButtonAds();
        }

        public void OnClick()
        {
            _panelWB.SetActive(false);
        }

        private void ButtonDefault()
        {
            _imageButton.sprite = _spriteButtonDefault;
            _imagePicture.sprite = _spriteUpdate;
        }
        
        private void ButtonAds()
        {
            _imageButton.sprite = _spriteButtonADS;
            _imagePicture.sprite = _spriteTV;
            _imagePicture.color = new Color(1f, 1f, 1f, 1f);

            Vector3 vector3 = new Vector3(1, 1, 1);
        }
    }
}
