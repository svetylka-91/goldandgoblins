using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class UIUpgradeCardScreen : MonoBehaviour
{
    [SerializeField] private OneCardUI _cardUI;
    [SerializeField] private UIRareLine _rareLine;

    [SerializeField] private TextMeshProUGUI _textEffect1;
    [SerializeField] private TextMeshProUGUI _textEffect2;

    private bool _posibleClose;

    public void GenerateScreen(OneCard card)
    {
        gameObject.SetActive(true);
        _posibleClose= false;
        DOTween.Sequence().SetDelay(1f).OnComplete(() => { _posibleClose = true; });

        _cardUI.InitCardByIndex(card.Index);
        _rareLine.GenerateLine(_cardUI);

        card.TryLevelUp();

        _textEffect1.text = card.GetCaptionEffect();
        _textEffect2.text = card.GetTextEffectCurrentLevel();

        EntryPoint.Instance.UIManager.GetCardsPanel().gameObject.SetActive(false);
    }

    private void Update()
    {
        if ((_posibleClose) && Input.GetMouseButtonUp(0))
            gameObject.SetActive(false);
    }
}
