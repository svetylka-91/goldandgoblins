﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class Level : MonoBehaviour
{
    [SerializeField] private int _levelForMines=1;

    private Dictionary<Vector2Int, DestroyableObject> _allDestroyableObjects = new Dictionary<Vector2Int, DestroyableObject>();
    private Dictionary<Vector2Int, PickableObject> _allPickableObjects = new Dictionary<Vector2Int, PickableObject>();
    private Dictionary<Vector2Int, BarrelScript> _allBarrelsObjects = new Dictionary<Vector2Int, BarrelScript>();

    public int LevelForMines { get => _levelForMines;}

    public void FinishIt()
    {
        DestroyImmediate(gameObject);
    }

    public bool IsPlaceFreeByPos(Vector2Int pos)
    {
        if (_allDestroyableObjects.ContainsKey(pos)) return false;
        if (_allPickableObjects.ContainsKey(pos)) return false;
        if (_allBarrelsObjects.ContainsKey(pos)) return false;
        return !EntryPoint.Instance.GoblinsManager.IsGoblinExistOnScene(pos);
    }
    public bool IsPlaceFreeByPos(int x, int y)
    {
        Vector2Int pos = new Vector2Int(x, y);
        return IsPlaceFreeByPos(pos);
    }



    #region PickableObjects
    public void AddPickableObjectFromScene(PickableObject pickableObject, Vector2Int pos)
    {
        if (!_allPickableObjects.ContainsKey(pos))
        {
            _allPickableObjects[pos] = pickableObject;
        }
    }
    public PickableObject getPickableObjectByPos(Vector2Int pos)
    {
        if (_allPickableObjects.ContainsKey(pos))
            return _allPickableObjects[pos];
        else
            return null;
    }
    public void DestroyPickableFromScene(Vector2Int pos)
    {
        _allPickableObjects.Remove(pos);
    }
    #endregion

    #region DestroyableObjects
    public DestroyableObject getDestroyableObjectByPos(Vector2Int pos)
    {
        if (_allDestroyableObjects.ContainsKey(pos))
            return _allDestroyableObjects[pos];
        else
            return null;
    }
    public void AddDestroyableObjectFromScene(DestroyableObject destroyableObject, Vector2Int pos)
    {
        if (!_allDestroyableObjects.ContainsKey(pos))
        {
            _allDestroyableObjects[pos] = destroyableObject;
        }
    }
    public void RemoveDestroyableObjectFromScene(Vector2Int pos)
    {
        _allDestroyableObjects.Remove(pos);
    }
    #endregion

    #region Barrels
    public void AddBarrelObjectFromScene(BarrelScript pickableObject, Vector2Int pos)
    {
        if (!_allBarrelsObjects.ContainsKey(pos))
        {
            _allBarrelsObjects[pos] = pickableObject;
        }
    }
    public void RemoveBarrelObjectFromScene(Vector2Int pos)
    {
        _allBarrelsObjects.Remove(pos);
    }
    #endregion
}
