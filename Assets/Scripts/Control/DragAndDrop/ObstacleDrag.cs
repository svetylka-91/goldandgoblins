﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ObstacleDrag : MonoBehaviour
{
    [Tooltip("")]
    [SerializeField] private Vector2 _size;
    [SerializeField] private List<Vector3> _allowablePosition = new List<Vector3>();

    private Transform _thisTransform;
    private BoxCollider _thisCollider;

    private Vector3 _offset;
    
    private void Awake()
    {
        if (_size == Vector2.zero)
        {
            _size = SetSizeFromCollider();
            Debug.LogWarning($"Size ObstacleDrag not set in {gameObject.name}. \n " +
                $"Was used values from ColliderBox: {_size.x};{_size.y}");
        }
        if (_size == Vector2.zero)
        {
            throw new System.Exception($"Size ObstacleDrag not set in {gameObject.name} can't equal 0;0");
        }

        _thisTransform = GetComponent<Transform>();

        _thisCollider = GetComponent<BoxCollider>();
        _thisCollider.center = new Vector3(
            x: _thisCollider.center.x,
            y: -0.02f,
            z: _thisCollider.center.z);
        _thisCollider.size = new Vector3(
            x: _thisCollider.size.x,
            y: 0f,
            z: _thisCollider.size.z);

        _offset = new Vector3(
            x: _thisCollider.center.x,
            y: -_thisTransform.position.y,
            z: _thisCollider.center.z);

        _allowablePosition.AddRange(FindAllowablePositions());
    }

    private Vector2 SetSizeFromCollider()
    {
        var sizeCollider = GetComponent<BoxCollider>().size;

        return new Vector2(
            x: Mathf.RoundToInt(sizeCollider.x), 
            y: Mathf.RoundToInt(sizeCollider.z));
    }

    private List<Vector3> FindAllowablePositions()
    {
        Vector3 thisPosition = _thisTransform.position + _offset;

        float distanceAllowablePositionX = (_size.x - 1f) / 2f + 1f;
        float distanceAllowablePositionY = (_size.y - 1f) / 2f + 1f;

        List<Vector3> seriesPositionsOneSize = new List<Vector3>();
        List<Vector3> seriesPositionsOppositeSize = new List<Vector3>();

        for (int i = 1, imax = (int)_size.y; i <= imax; i++)
        {
            var positionOneSize = new Vector3(
                x: thisPosition.x - distanceAllowablePositionX,
                y: thisPosition.y,
                z: thisPosition.z - distanceAllowablePositionY + i);

            if(IsHavePlaneUnder(positionOneSize) == true)
            {
                seriesPositionsOneSize.Add(positionOneSize);
            }

            var positionOppositeSize = new Vector3(
                x: thisPosition.x + distanceAllowablePositionX,
                y: thisPosition.y,
                z: thisPosition.z + distanceAllowablePositionY - i);

            if (IsHavePlaneUnder(positionOppositeSize) == true)
            {
                seriesPositionsOneSize.Add(positionOppositeSize);
            }
        }

        for (int i = 1, imax = (int)_size.x; i <= imax; i++)
        {
            var positionOneSize = new Vector3(
                x: thisPosition.x - distanceAllowablePositionX + i,
                y: thisPosition.y,
                z: thisPosition.z - distanceAllowablePositionY);

            if (IsHavePlaneUnder(positionOneSize) == true)
            {
                seriesPositionsOneSize.Add(positionOneSize);
            }

            var positionOppositeSize = new Vector3(
                x: thisPosition.x + distanceAllowablePositionX - i,
                y: thisPosition.y,
                z: thisPosition.z + distanceAllowablePositionY);

            if (IsHavePlaneUnder(positionOppositeSize) == true)
            {
                seriesPositionsOneSize.Add(positionOppositeSize);
            }
        }

        List<Vector3> result = new List<Vector3>();
        result.AddRange(seriesPositionsOneSize);
        result.AddRange(seriesPositionsOppositeSize);

        return result;
    }


    public bool TryGetNearestAllowablePosition(Vector3 from, out Vector3 nearestAllowablePosition)
    {
        nearestAllowablePosition = Vector3.zero;
        bool result = false;

        var leastDistance = float.MaxValue;

        foreach(var item in _allowablePosition)
        {
            if (IsFreePosition(item) == true)
            {
                result = true;

                var distance = Vector3.Distance(from, item);
                if (distance < leastDistance)
                {
                    leastDistance = distance;
                    nearestAllowablePosition = item;
                }
            }
        }

        return result;
    }

    private bool IsHavePlaneUnder(Vector3 position)
    {
        var rayCasts = Physics.RaycastAll(position, Vector3.down);
        return rayCasts.Any((x) => x.collider.TryGetComponent<Plane>(out _));
    }

    private bool IsFreePosition(Vector3 position)
    {
        return Physics.Raycast(position + new Vector3(0f, -0.2f, 0f), Vector3.up, out _) == false;
    }
}
