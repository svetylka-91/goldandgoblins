﻿using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Goblin))]
[RequireComponent(typeof(MoveningSwap))]
public class MoveningHandler : MonoBehaviour
{
    [SerializeField] private GameCell _cell;
    
    private Goblin _thisGoblin;
    private Goblin _otherGoblin;
    

    private Movement _movement;

    private MoveningSwap _moveningSwap;
    private MoveningSwap _otherMoveningSwap;

    private Transform _thisTransform;

    private Vector3 _startPosition;
    private Vector3 _allowablePosition;
    private Vector3 _swapedPosition;
    private Vector3 _thisCenterPosition;

    private DestroyableObject _selectedDestroyableObject;

    private bool _isDrag;
    private bool _isPostMove;

    private RaycastHit[] _raycastHits;


    private bool _isPlaySoundMerge;
    private bool _isStopSoundMerge;

    private void Awake()
    {
        _thisTransform = GetComponent<Transform>();
        _movement = GetComponent<Movement>();
        _thisGoblin = GetComponent<Goblin>();
        _moveningSwap = GetComponent<MoveningSwap>();

        if (_cell == null)
            _cell = FindObjectOfType<GameCell>();
        if (_cell == null)
            _cell = new InstantiaterPrefab<GameCell>().Create(transform.parent);
        if (_cell == null)
            Debug.LogError($"Game Cell to {name} not linked or not created.");

        _startPosition = _thisTransform.position.RountToIntXZ();
        _allowablePosition = _startPosition;

        _isPlaySoundMerge = true;
        _isStopSoundMerge = false;
    }

    public void SetStartPosition()
    {
        _thisGoblin.OnStartMove();

        _startPosition = _thisTransform.position.RountToIntXZ();
        _allowablePosition = _startPosition;

        _isDrag = true;
    }

    public void FindAllowablePosition()
    {
        _selectedDestroyableObject = null;
        _thisCenterPosition = _thisTransform.position.RountToIntXZ();
        _cell?.SetColorOnMove();

        _raycastHits = Physics.RaycastAll(_thisCenterPosition, Vector3.down, 2f);
        bool hasRaycastHitIsPlane = _raycastHits.Any((x) => x.collider.TryGetComponent<Plane>(out _));

        if (_raycastHits.Length != 0 && hasRaycastHitIsPlane)
        {
            _allowablePosition = _thisCenterPosition;

            ObstacleDrag obstacleDrag = null;
            if (_raycastHits.Any((x) => x.collider.TryGetComponent(out obstacleDrag)) == true)
            {
                if (obstacleDrag.TryGetNearestAllowablePosition(_thisTransform.position, out Vector3 position))
                {
                    _allowablePosition = position;

                    if (obstacleDrag.TryGetComponent(out DestroyableObject destroyableObject))
                    {
                        _cell?.Move(_allowablePosition);
                        _selectedDestroyableObject = destroyableObject;
                        _cell?.SetStateForTookStone(_thisCenterPosition, EntryPoint.Instance.LevelsManager.CurrentLevel.getDestroyableObjectByPos(destroyableObject.GetPosition()).GetTargetForGoblins());
                    }

                }
                else
                {
                    _allowablePosition = _startPosition;
                }
            }
        }

        if (_otherGoblin != null && _thisCenterPosition != _swapedPosition)
        {
            if (_otherGoblin.IsCanMerge(_thisGoblin) == false)
            {
                if (_otherGoblin.TryGetComponent(out MoveningSwap goblinToSwap))
                {
                    goblinToSwap.MoveToInitialPosition();
                    _swapedPosition = Vector3.zero;

                    _otherGoblin = null;
                }
            }
        }

        if (_otherGoblin != null && _thisCenterPosition == _otherGoblin.transform.position)
        {
            if (_otherGoblin.IsCanMerge(_thisGoblin))
            {
                _cell?.SetMergeStateTrue();
                _cell?.SetColorOnClear();

                if (_isPlaySoundMerge)
                {
                    GoblinAudioManager.PlaySoundMergeTarget();
                    _isPlaySoundMerge = false;
                    _isStopSoundMerge = true;
                }

            }
        }
        else if(_isStopSoundMerge)
        {
            _isPlaySoundMerge = true;
            GoblinAudioManager.StopSoundMergeTarget();
            _isStopSoundMerge = false;
            _cell?.SetMergeStateFalse();
        }


        _cell?.Move(_allowablePosition);
    }

    public void MoveToAllowablePosition()
    {
        bool arrowIsSet = false;

        if (_raycastHits.Length != 0)
        {
            foreach (var item in _raycastHits)
            {
                if (item.collider.TryGetComponent(out DestroyableObject destroyableObject))
                {
                    //_selectedDestroyableStone = destroyableStone;
                }
            }
        }

        _movement.MoveTo(_allowablePosition);
        _cell?.SetColorOnClear();
        

        _isDrag = false;

        TryMerge();
        _moveningSwap.FixInitialPosition(_allowablePosition);

        if (_otherMoveningSwap != null)
        {
            if (_otherGoblin != null)
            {
                _otherGoblin.RemoveFromSceneOnPosition(_swapedPosition);
                if (!arrowIsSet)
                    _otherGoblin.OnChangePos(_otherGoblin.transform.position);
            }

            _otherMoveningSwap.FixInitialPosition();
        }

        _swapedPosition = Vector3.zero;
        _otherGoblin = null;
        _otherMoveningSwap = null;

        _isPostMove = true;
        //_thisGoblin.OnChangePos(_allowablePosition);
    }

    private void TryMerge()
    {
        if (_otherGoblin != null && _thisCenterPosition == _otherGoblin.transform.position)
        {
            if (_thisGoblin.IsCanMerge(_otherGoblin) == true)
            {
                _otherGoblin.OnWasMerged();
                _otherGoblin = null;
                _thisGoblin.Merge();
                _cell?.SetMergeStateFalse();
            }
        }
    }

    private void OnEnable()
    {
        _movement.OnEnd += ChangePositionInLevel;
    }
    private void OnDisable()
    {
        _movement.OnEnd -= ChangePositionInLevel;
    }

    private void ChangePositionInLevel()
    {
        if (_isPostMove == false) return;
        _thisGoblin.OnChangePos(_allowablePosition);

        if (_selectedDestroyableObject != null)
        {
            _thisGoblin.TryFindNewTarget(true, _selectedDestroyableObject.GetPosition());
        }
        _isPostMove = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isDrag == false) return;

        if (other.TryGetComponent(out Goblin otherGoblin))
        {
            if (otherGoblin.IsCanMerge(_thisGoblin) == true)
            {

            }
            else
            {
                //otherGoblin.ShowVXMerge();
            }
        }
    }
    //cringe
    private void OnTriggerStay(Collider other)
    {
        if (_isDrag == false) return;

        if (other.TryGetComponent(out Goblin otherGoblin))
        {
            var otherGoblinPosition = otherGoblin.transform.position.RountToIntXZ();

            if (otherGoblinPosition == _thisCenterPosition)
            {
                _otherGoblin = otherGoblin;

                if (otherGoblin.IsCanMerge(_thisGoblin) == false)
                {
                    if (other.TryGetComponent(out MoveningSwap goblinToSwap))
                    {
                        if (goblinToSwap.IsInitialPosition(_thisCenterPosition))
                        {
                            if (_otherMoveningSwap != null)
                            {
                                _otherMoveningSwap.MoveToInitialPosition();
                            }

                            _otherMoveningSwap = goblinToSwap;
                            _swapedPosition = _thisCenterPosition;
                            goblinToSwap.MoveToPresumptivePosition(_startPosition);
                        }
                    }
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (_isDrag == false) return;

        if (other.TryGetComponent(out Goblin otherGoblin))
        {
            if (otherGoblin.IsCanMerge(_thisGoblin) == true)
            {
                //otherGoblin.HideVXMerge();
            }
        }
    }
}
