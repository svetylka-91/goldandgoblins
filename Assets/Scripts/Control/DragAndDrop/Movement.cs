﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Goblin))]
public class Movement : MonoBehaviour
{
    public event Action OnEnd;

    private Transform _thisTransform;
    private Vector3 _currentPositionToMove;

    private float _time = 0f;

    private void Awake()
    {
        _thisTransform = GetComponent<Transform>();
    }

    public void MoveTo(Vector3 position)
    {
        _time = 0.33f;

        StopAllCoroutines();
        StartCoroutine(Move(position));
    }

    private IEnumerator Move(Vector3 position)
    {
        var timeToMove = 0f;
        var speed = 1f / _time;
        var startPosition = _thisTransform.position;
        var deltaTime = Time.deltaTime;

        while (_time > 0f)
        {
            _time -= deltaTime;
            timeToMove += deltaTime * speed;

            _thisTransform.position = Vector3.Lerp(startPosition, position, timeToMove);

            yield return null;
        }

        _thisTransform.position = position;
        OnEnd?.Invoke();

        yield return null;
    }
}
