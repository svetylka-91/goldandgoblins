using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{


    [Header("Top panels")]
    [SerializeField] private UIUpperPanelLevel _uiUpperPanelLevel;
    [SerializeField] private UIUpperPanelCurrency _uiUpperPanelCurrency;

    [Header("Panels")]
    [SerializeField] public UIBottomPanel UIBottomPanel;
    [SerializeField] private CardsPanel _cardsPanel;
    [SerializeField] public  MarketUI MarketUI;
    [SerializeField] private GameObject _gainADS;
    [SerializeField] private GameObject _coinDelivery;
    [SerializeField] private GameObject _settingsPanel;
    [SerializeField] private AdsWasShowScreen _adsWasShowScreen;

    [Header("Chests")]

    [SerializeField] private OpenChestScreenUI _openChestScreenUI;
    [SerializeField] public UIMineChest _uiMineChest;

    [Header("Images")]
    [SerializeField] private Image _imageChestButton;
    [SerializeField] private List<Sprite> _listImageColoredBtns;

    [Header("Scripts")]
    [SerializeField] private PanelClckedOnCard _panelClckedOnCard;
    [SerializeField] private PanelMineAutomatization _panelMineAutomatization;
    [SerializeField] private UIUpgradeCardScreen _uiUpgradeCardScreen;

    private bool _needStopCamera;

    public void Init()
    {
        _uiUpperPanelCurrency.Init();
        _uiUpperPanelLevel.Init();
        UIBottomPanel.Init();
        MarketUI.Init();
    }


    public bool IsNeedStopCamera { get { return _needStopCamera; } }

    public void ShowAds(Action callback) => _adsWasShowScreen.ShowAds().Callback(callback);

    public void ResetSceens()
    {
        _cardsPanel.gameObject.SetActive(false);
        MarketUI.gameObject.SetActive(false);
        _gainADS.SetActive(false);
        _settingsPanel.SetActive(false);
        _needStopCamera = false;
    }

    public void ShowGainADS()
    {
        ResetSceens();
        _gainADS.SetActive(true);
        _needStopCamera = true;
    }

    public void ShowOpenChestScreenUI(ChestType chest, int bottles)
    {
        _openChestScreenUI.gameObject.SetActive(true);
        _openChestScreenUI.Init(chest, bottles);
        _needStopCamera = true;
    }
    public void ShowCardsPanel()
    {
        ResetSceens();
        _cardsPanel.RegenAndShow();
        _needStopCamera=true;
    }
    public void ShowMarketPanel()
    {
        ResetSceens();
        MarketUI.gameObject.SetActive(true);
        _needStopCamera=true;
    }
    public void ShowSettingsPanel()
    {
        ResetSceens();
        _settingsPanel.SetActive(true);
        _needStopCamera = true;
    }


    public void ClickedOnCard(int nomer) => _panelClckedOnCard.GenerateScreen(nomer);
    public PanelMineAutomatization GetPanelMineAutomatization() => _panelMineAutomatization;
    public UIUpgradeCardScreen GetUiUpgradeCardScreen() => _uiUpgradeCardScreen;
    public CardsPanel GetCardsPanel() => _cardsPanel;
}
