using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curency
{
    private double _count;
    public double Count { get { return _count; } }
    public double OnLoadCount { 
        set { 
            _count = value; 
            OnChangeCount?.Invoke(_count); 
        } 
    }

    public event Action<double> OnChangeCount; 
    public void ChangeCount(double change)
    {
        _count += change;
        OnChangeCount?.Invoke(_count);
    }
}

public class CurrencyManager
{
    public Curency Coins;
    public Curency Bottles;
    public Curency Gems;

    public CurrencyManager()
    {
        Coins = new Curency();
        Bottles = new Curency();
        Gems = new Curency();
    }
}
