using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardsManager
{
    public AllCards _cards;

    public event Action<int> OnChangeCountCardsPosibleUp;
    public event Action OnChangeCards;
    public CardsManager()
    {
        Cards = new AllCards();
        EntryPoint.Instance.CurrencyManager.Bottles.OnChangeCount += OnChangeCountBottles;
    }

    public AllCards Cards
    {
        get { return _cards; }
        set { _cards = value; }
    }

    public void OnDestroy()
    {
        EntryPoint.Instance.CurrencyManager.Bottles.OnChangeCount -= OnChangeCountBottles;
    }

    public string GetRareText(RareCard rare)
    {
        return rare switch
        {
            RareCard.Common => "�������",
            RareCard.Uncommon => "���������",
            RareCard.Rare => "������",
            RareCard.Unique => "�����������",
            _ => "�������",
        };
    }
    public OneCard GetCardForChest(RareCard rare)
    {
        List<OneCard> list = new List<OneCard>();

        foreach (var item in Cards.AllCardsData.Values)
        {
            if (item.Rare != rare) continue;
            if (item.RoundForOpen <= EntryPoint.Instance.LevelsManager.NomerLevel)
                list.Add(item);
        }

        return list[UnityEngine.Random.Range(0,list.Count)];
    }

    private void OnChangeCountBottles(double count) => RecountCardsPosibleUp();
    private void RecountCardsPosibleUp()
    {
        int countCardsPosibleUp = 0;
        foreach (var item in Cards.AllCardsData.Values)
        {
            if (item.IsPosibleLevelUpNow())
                countCardsPosibleUp++;
        }
        OnChangeCountCardsPosibleUp?.Invoke(countCardsPosibleUp);
    }

    public TypeCard GetTypeCardByTypeMine(Gems mine)
    {
        return mine switch
        {
            Gems.amethist => TypeCard.amethist,
            Gems.citraine => TypeCard.citraine,
            Gems.agate => TypeCard.agate,
            Gems.topaz => TypeCard.topaz,
            Gems.opal => TypeCard.opal,
            _ => throw new NotImplementedException("not exist gem"),
        };
    }

    public int GetIndex(Gems type, int rare)
    {
        RareCard rareCard = 
            rare == 0 ? 
            RareCard.Common : 
            RareCard.Uncommon;

        foreach (var item in Cards.AllCardsData.Values)
        {
            if ((item.Rare == rareCard) &&
                item.Type == GetTypeCardByTypeMine(type))
                return item.Index;
        }

        Debug.LogError("not finded index for mine");
        return 0;
    }

    public int GetBonusLimitFromCards()
    {
        int value = 0;
        foreach (var item in Cards.AllCardsData.Values)
        {
            if (item.EffectCard == EffectCard.goblinLimit)
                value += item.GetValueEffectWithCurrentLevel();
        }
        return value;
    }
    public int GetBonusForgeFromCard()
    {
        return Cards.AllCardsData[0].GetValueEffectWithCurrentLevel();
    }

    public float GetBonusRockFromCards()
    {
        float value = 0;
        foreach (var item in Cards.AllCardsData.Values)
        {
            if (item.EffectCard == EffectCard.rockIncome)
                value += item.GetValueEffectWithCurrentLevel()*0.01f;
        }
        return value;
    }
}
