﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LevelsManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _levels;
    private Cannon _cannon;

    public int NomerLevel = 0;
    public Level CurrentLevel;

    public event Action<int> OnChangeLevel;

    public void Init()
    {
        NomerLevel = 0;
        TryMakeNextLevel();
    }

    public Cannon GetCannon()
    {
        if (_cannon==null) 
            _cannon = FindObjectOfType<Cannon>();

        return _cannon;
    }

    public void TryMakeNextLevel()
    {
        NomerLevel++;
        if ((NomerLevel < 1) || (NomerLevel > _levels.Length)) NomerLevel = 1;

        DestroyExistLevel();
        MakeLevel();
    }
    private void MakeLevel()
    {
        if (_levels.Length <= NomerLevel - 1)
        {
            Debug.Log("No finded levels for loading");
            return;
        }

        EntryPoint.Instance.GoblinsManager.ResetWhenStartNewLevel();
        EntryPoint.Instance.MineSidesManager.ResetWhenStartNewLevel();
        CurrentLevel = GetLoadLevel(_levels[NomerLevel - 1]).GetComponent<Level>();
        
    }
    private void DestroyExistLevel()
    {
        if (CurrentLevel!=null)
            CurrentLevel.FinishIt();
    }

    public GameObject GetLoadLevel(GameObject gameObjectPrefab)
    {
        GameObject newLevel = Instantiate(gameObjectPrefab);
        OnChangeLevel?.Invoke(NomerLevel);
        return newLevel;
    }

}
