using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Gems
{
    amethist,
    citraine,
    agate,
    topaz,
    opal
}

public struct DemandsCardForAuto
{
    public OneCard Card;
    public int Level;
}


public class MineSideData
{
    public int Level { get; set; }
    public double CurrentMiningTime { get; set; }
    public bool IsAuto { get; set; }

}


public class MineSidesManager
{
    private Dictionary<Gems, MineSideData> _allMineSideData;
    private Dictionary<Gems, MineSide> _allMineSideOnLevel;

    public MineSidesManager()
    {
        ResetData();
    }

    public void AddMineFromScene(Gems type, MineSide mineSide)
    {
        if (!_allMineSideOnLevel.ContainsKey(type))
        {
            _allMineSideOnLevel[type] = mineSide;
        }
    }

    public void ResetData()
    {
        _allMineSideOnLevel = new Dictionary<Gems, MineSide>();
        _allMineSideData = new Dictionary<Gems, MineSideData>();

        foreach (Gems item in Enum.GetValues(typeof(Gems)))
        {
            MineSideData _data = new()
            {
                CurrentMiningTime = 0,
                Level = 1
            };

            _allMineSideData[item] = _data;
        }
    }
    public void OnLoadData()
    {
        foreach (var item in _allMineSideOnLevel)
        {
            item.Value.OnSetLevel(_allMineSideData[item.Key].Level);
        }
    }
    public void ResetWhenStartNewLevel() => ResetData();

    public Dictionary<Gems, MineSideData> AllMineSideData
    {
        get { 
            return _allMineSideData; 
        }
        set { 
            _allMineSideData = value; 
        }
    }

    public string GetCaptionBeGem(Gems gem)
    {
        return gem switch
        {
            Gems.amethist => "�������",
            Gems.citraine => "������",
            Gems.agate => "����",
            Gems.topaz => "�����",
            Gems.opal => "����",
            _ => throw new System.NotImplementedException(),
        };
    }

    //TODO �������� ��� �� ������������ ��� �������
    public int GetLevel(Gems type) => _allMineSideData[type].Level;
    public void SetLevel(Gems type, int value) => _allMineSideData[type].Level = value;

    public DemandsCardForAuto GetDemandsCardForAuto(Gems type)
    {
        int level = 1+EntryPoint.Instance.LevelsManager.CurrentLevel.LevelForMines / 2;
        if (level < 1) level = 1;

        return
            new DemandsCardForAuto()
            {
                Level = level,
                Card = EntryPoint.Instance.CardsManager.Cards.AllCardsData[EntryPoint.Instance.CardsManager.GetIndex(type, (EntryPoint.Instance.LevelsManager.CurrentLevel.LevelForMines+1) % 2)]
            };
    }

    public int GetMyltyplyIncome(Gems TypeMine)
    {
        int value = 1;
        TypeCard typeCard = EntryPoint.Instance.CardsManager.GetTypeCardByTypeMine(TypeMine);        

        foreach (var item in EntryPoint.Instance.CardsManager.Cards.AllCardsData.Values)
        {
            if (item.Type == typeCard)
                value *= item.GetValueEffectWithCurrentLevel();
        }

        return value;
    }
    public void SetAutomatization(Gems TypeMine) => _allMineSideOnLevel[TypeMine].SetAutomatization();

}
