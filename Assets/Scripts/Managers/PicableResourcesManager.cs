using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicableResourcesManager : MonoBehaviour
{
    [SerializeField] private GameObject _coreCoin;
    [SerializeField] private GameObject _softCurrency;
    [SerializeField] private GameObject _hardCurrency;

    public void MakeGoldOnScene(int lvl, Vector2Int pos)
    {
        double countCoins = 7.5 * (lvl + 1) * Balance.ForgeMoneyIncomePerTime 
            * (1+EntryPoint.Instance.CardsManager.GetBonusRockFromCards());
        Instantiate(_coreCoin, new Vector3(pos.x, 0, pos.y), Quaternion.identity, EntryPoint.Instance.LevelsManager.CurrentLevel.transform).GetComponent<GoldCoin>().SetCount(countCoins);
    }

    public void MakeSoftCurrencyOnScene(int count, Vector2Int pos)
    {
        Instantiate(_softCurrency, new Vector3(pos.x, 0, pos.y), Quaternion.identity, EntryPoint.Instance.LevelsManager.CurrentLevel.transform).GetComponent<SoftCurrency>().SetCount(count);
    }
    public void MakeHardCurrencyOnScene(int count, Vector2Int pos)
    {
        Instantiate(_hardCurrency, new Vector3(pos.x, 0, pos.y), Quaternion.identity, EntryPoint.Instance.LevelsManager.CurrentLevel.transform).GetComponent<HardCurrency>().SetCount(count);
    }
}
