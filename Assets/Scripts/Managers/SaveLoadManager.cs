using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Newtonsoft.Json;

public class SL_objs
{
    public Dictionary<string, object> Objs;
    public SL_objs(string _load)
    {
        Objs = JsonConvert.DeserializeObject<Dictionary<string, object>>(_load);
    }

    private void Unpars<T>(ref T _ob, string i)
    {
        if (Objs.ContainsKey(i))
        {
            if (_ob is bool)
            {
                _ob = JsonConvert.DeserializeObject<T>(Objs[i].ToString().ToLower());
            }
            else if (_ob is float)
            {
                _ob = JsonConvert.DeserializeObject<T>(Objs[i].ToString().Replace(',','.'));
            }
            else
            {
                _ob = JsonConvert.DeserializeObject<T>(Objs[i].ToString());
            }
                
        }
        else
            Debug.Log("�������� �������� �������� ������ ������ ��� ���� � �����.");
    }
    private T Unpars<T>(string i)
    {
        T checkType = default(T);
        if (Objs.ContainsKey(i))
        {
            if (checkType is bool)
            {
                return JsonConvert.DeserializeObject<T>(Objs[i].ToString().ToLower());
            }
            else if (checkType is float)
            {
                return JsonConvert.DeserializeObject<T>(Objs[i].ToString().Replace(',', '.'));
            }
            else
            {
                return JsonConvert.DeserializeObject<T>(Objs[i].ToString());
            }

        }
        else
        {
            Debug.Log("�������� �������� �������� ������ ������ ��� ���� � �����.");
            return default(T);         
        }
    }

    internal void Load(ref List<string> _ob, string v)
    {
        Unpars<List<string>>(ref _ob, v);
    }

    internal T Load<T>(string v) => Unpars<T>(v);

    internal void Load(ref double _ob, string v)
    {
        Unpars<double>(ref _ob, v);
    }
    internal void Load(ref int _ob, string v)
    {
        Unpars<int>(ref _ob, v);
    }
    internal void Load(ref float _ob, string v)
    {
        Unpars<float>(ref _ob, v);
    }
    internal void Load(ref long _ob, string v)
    {
        Unpars<long>(ref _ob, v);
    }
    internal void Load(ref string _ob, string v)
    {
        Unpars<string>(ref _ob, v);
    }
    internal void Load(ref bool _ob, string v)
    {
        Unpars<bool>(ref _ob, v);
    }

    internal void Load(ref SaveLoadData _ob, string v)
    {
        Unpars<SaveLoadData>(ref _ob, v);
    }
}

public class SaveLoadManager
{
    public Dictionary<string, object> SL;
    public SL_objs SL_obj;
    public SaveLoadData SLData = new SaveLoadData();

    public void MakeSave()
    {
        SL = new Dictionary<string, object>();

        SL.Add("TimeForFullFreeChests", EntryPoint.Instance.ChestsManager.TimeForFullFreeChests);
        SL.Add("SLData", SLData);

        string whole_file = JsonConvert.SerializeObject(SL);
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/params.save");
        writer.Write(whole_file);
        writer.Close();

        //Debug.Log(Application.persistentDataPath + "/params.save");
    }

    public bool CheckExistLoadingFile()
    {
        return File.Exists(Application.persistentDataPath + "/params.save");
    }
    public IEnumerator RegularSaving()
    {
        while (true)
        {
            MakeSave();
            yield return new WaitForSeconds(5f);
        }
    }

    public void MakeLoad()
    {
        if (File.Exists(Application.persistentDataPath + "/params.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/params.save", FileMode.Open);

            StreamReader reader = new StreamReader(file);
            string loadDate = reader.ReadToEnd();
            if (loadDate.Length > 1)
            {
                SL_obj = new SL_objs(loadDate);

                EntryPoint.Instance.ChestsManager.TimeForFullFreeChests = SL_obj.Load<int>("TimeForFullFreeChests");
                SL_obj.Load(ref SLData, "SLData");


                SLData.ApplyLoadData();
                //Debug.Log(EntryPoint.Instance.MineSidesManager.GetLevel(Gems.amethist));
            }
            file.Close();
        }

    }
 
}
