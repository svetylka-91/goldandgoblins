﻿using UnityEngine;
using System.Collections;
using System;

public enum TypeCard
{
    forge,
    rocks,
    mineshafts,
    goblins,
    amethist,
    citraine,
    agate,
    topaz,
    opal,
    jade, //нефрит
    onix,
    checkpoints,
    saphire,
    tormaline,
    deliveries,
    aquamarine,
    emerald,
    diamond

    //diamond открывается на 47 уровне. и хватит пожалуй.

}
public enum CaptionCard
{
    income = 0,
    speed = 1,
    limit = 2,
    timeToApear = 3,
    discount = 4,
    critChance = 5,
    critDamage = 6,
    levelPlus = 7
}
public enum RareCard
{
    Common,
    Uncommon,
    Rare,
    Unique
}
public enum EffectCard
{
    income,
    rockIncome,
    timeToAppear,
    speedSideMine,
    incomeSideMine,
    goblinLimit
}


public class OneCard
{
    private int _countHaveCards = 0;

    public int Index = 0;
    public int RoundForOpen;
    public int Level = 0;
    public string LongCaption;
    public CaptionCard Caption;
    public TypeCard Type;
    public RareCard Rare;
    public EffectCard EffectCard;

    public OneCard(int index, CaptionCard caption, TypeCard type, int roundForOpen, RareCard rare = RareCard.Common, string longCaption = "", EffectCard effectCard = EffectCard.income)
    {
        Index = index;
        Rare = rare;

        Caption = caption;
        Type = type;

        RoundForOpen = roundForOpen;
        LongCaption = longCaption;
        EffectCard = effectCard;
    }

    public int CountHaveCards
    {
        set
        {
            _countHaveCards = value;
            if ((_countHaveCards > 0) && (Level == 0)) Level = 1;
        }
        get { return _countHaveCards; }
    }

    private int GetValueEffect(int level)
    {
        switch (EffectCard)
        {
            case EffectCard.income:
                if (Rare == RareCard.Common)
                    return (int)Math.Pow(3, level);
                else
                    return (int)Math.Pow(4, level);
            case EffectCard.rockIncome:
                return (int)(60 * Mathf.Pow(level, 2));
            case EffectCard.timeToAppear:
                return level;
            case EffectCard.speedSideMine:
                return level * 11;
            case EffectCard.incomeSideMine:
                return (int)Math.Pow(3, level);
            case EffectCard.goblinLimit:
                return level;
            default:
                Debug.LogError("not exist type");
                return 0;
        }
    }
    public int GetValueEffectWithCurrentLevel() => GetValueEffect(Level);

    private string GetTextEffectByLevel(int level)
    {
        switch (EffectCard)
        {
            case EffectCard.income:
                return $"X{GetValueEffect(level)}";
            case EffectCard.rockIncome:
                return $"+{GetValueEffect(level)}%";
            case EffectCard.timeToAppear:
                return $"-{GetValueEffect(level)}";
            case EffectCard.speedSideMine:
                return $"+{GetValueEffect(level)}%";
            case EffectCard.incomeSideMine:
                return $"X{GetValueEffect(level)}";
            case EffectCard.goblinLimit:
                return $"+{GetValueEffect(level)}";
            default:
                Debug.LogError("not exist type");
                return "";
        }
    }

    public string GetTextEffectCurrentLevel() => GetTextEffectByLevel(Level);
    public string GetTextEffectNextLevel() => GetTextEffectByLevel(Level + 1);

    public string GetCaptionEffect()
    {
        switch (EffectCard)
        {
            case EffectCard.income: 
                return "доход";
            case EffectCard.rockIncome:
                return "доход от породы";
            case EffectCard.timeToAppear:
                return "время до появления гоблина";
            case EffectCard.speedSideMine: 
                return "скорость работы шахт";
            case EffectCard.incomeSideMine: 
                return "доход от шахт";
            case EffectCard.goblinLimit:
                return "лимит гоблинов";
            default:
                Debug.LogError("not exist type");
                return "";
        }
    }

    public string GetDiscription()
    {
        return Type switch
        {
            TypeCard.forge => "Кузница приносит больше дохода",
            TypeCard.rocks => "Из породы можно добыть больше монет",
            TypeCard.mineshafts => "Улучшает работу боковых шахт",
            TypeCard.goblins => "Улучшает работу гоблинов",
            TypeCard.amethist => "Шахта Аметист приносит больше дохода и автоматизируется",
            TypeCard.citraine => "Шахта Цитраин приносит больше дохода и автоматизируется",
            TypeCard.agate => "Шахта Агат приносит больше дохода и автоматизируется",
            TypeCard.topaz => "Шахта Топаз приносит больше дохода и автоматизируется",
            TypeCard.opal => "Шахта Опал приносит больше дохода и автоматизируется",
            TypeCard.jade => "Шахта Нефрит приносит больше дохода и автоматизируется",
            TypeCard.onix => "Шахта Оникс приносит больше дохода и автоматизируется",
            TypeCard.checkpoints => "Дает крутые бонусы",
            TypeCard.saphire => "Шахта Сапфир приносит больше дохода и автоматизируется",
            TypeCard.tormaline => "Шахта Турмлин приносит больше дохода и автоматизируется",
            TypeCard.deliveries => "Ускоряет доставку",
            TypeCard.aquamarine => "Шахта Аквамарин приносит больше дохода и автоматизируется",
            TypeCard.emerald => "Шахта Изумруд приносит больше дохода и автоматизируется",
            TypeCard.diamond => "Шахта Алмаз приносит больше дохода и автоматизируется",
            _ => throw new System.NotImplementedException(),
        };

    }
    public string GetRareText() => EntryPoint.Instance.CardsManager.GetRareText(Rare);
    public string GetFileNameImage()
    {
        int tempNomer = Index % 15;
        return "NewCardImages\\ca" + tempNomer.ToString();
    }
    public string GetTextRoundNeedForOpen()=> "Рудник " + RoundForOpen.ToString();
    public string GetTextLevel() => "Уровень " + Level.ToString();
    public int GetIndexTypeImage()
    {
        //in OneCardUI have List<Sprite> _listImageTypeCard
        return Type switch
        {
            TypeCard.forge => 2,
            TypeCard.rocks => 1,
            TypeCard.goblins => 0,
            TypeCard.amethist => 2,
            TypeCard.citraine => 2,
            TypeCard.agate => 2,
            TypeCard.topaz => 2,
            TypeCard.opal => 2,
            TypeCard.jade => 2,
            TypeCard.onix => 2,
            TypeCard.saphire => 2,
            TypeCard.tormaline => 2,
            TypeCard.aquamarine => 2,
            TypeCard.emerald => 2,
            TypeCard.diamond => 2,

            //maybe need new icons
            TypeCard.mineshafts => 2,
            TypeCard.checkpoints => 1,
            TypeCard.deliveries => 0,
            _ => 0,
        };
    }
    public string GetTypeText()
    {
        switch (Type)
        {
            case TypeCard.forge:
                return "Кузница";
            case TypeCard.rocks:
                return "Порода";
            case TypeCard.mineshafts:
                return "Шахты";
            case TypeCard.goblins:
                return "Гоблины";
            case TypeCard.amethist:
                return "Аметист";
            case TypeCard.citraine:
                return "Цитрин";
            case TypeCard.agate:
                return "Агат";
            case TypeCard.topaz:
                return "Топаз";
            case TypeCard.opal:
                return "Опал";
            case TypeCard.jade:
                return "Нефрит";
            case TypeCard.onix:
                return "Оникс";
            case TypeCard.checkpoints:
                return "Рубежи";
            case TypeCard.saphire:
                return "Сапфир";
            case TypeCard.tormaline:
                return "Турмалин";
            case TypeCard.deliveries:
                return "Доставки";
            case TypeCard.aquamarine:
                return "Аквамарин";
            case TypeCard.emerald:
                return "Изумруд";
            case TypeCard.diamond:
                return "Алмаз";
            default:
                Debug.LogError("Ошибка");
                return "Кузница";
        }
    }
    public string GetShortCaptionText()
    {
        switch (Caption)
        {
            case CaptionCard.income:
                return "Доход";
            case CaptionCard.speed:
                return "Скорость";
            case CaptionCard.limit:
                return "Лимит+";
            case CaptionCard.timeToApear:
                return "Время до появления";
            case CaptionCard.discount:
                return "Скидка";
            case CaptionCard.critChance:
                return "Шанс критического удара";
            case CaptionCard.critDamage:
                return "Критический урон";
            case CaptionCard.levelPlus:
                return "Уровень+";

            default:
                Debug.LogError("Ошибка");
                return "Доход";
        }
    }
    public int GetCountNeedBottles()
    {
        switch (Rare)
        {
            case RareCard.Common:
                {
                    switch (Level)
                    {
                        case 0: return 50;
                        case 1: return 100;
                        case 2: return 200;
                        case 3: return 400;
                        case 4: return 800;
                        case 5: return 1500;
                        default:
                            return (int)(1500 * Math.Pow(2, Level - 5));
                    }
                }
            case RareCard.Uncommon:
                {
                    switch (Level)
                    {
                        case 0: return 100;
                        case 1: return 200;
                        case 2: return 400;
                        case 3: return 800;
                        case 4: return 1500;
                        default:
                            return (int)(1500 * Math.Pow(2, Level - 4));
                    }
                }
            case RareCard.Rare:
                {
                    switch (Level)
                    {
                        case 0: return 200;
                        case 1: return 400;
                        case 2: return 800;
                        case 3: return 1500;
                        default:
                            return (int)(1500 * Math.Pow(2, Level - 3));
                    }
                }
            case RareCard.Unique:
                {
                    switch (Level)
                    {
                        case 0: return 400;
                        case 1: return 800;
                        case 2: return 1500;
                        default:
                            return (int)(1500 * Math.Pow(2, Level - 2));
                    }
                }
            default: return 0;
        }
    }
    public int GetCountNeedCards()
    {
        switch (Level)
        {
            case 0: return 2;
            case 1: return 5;
            case 2: return 10;
            case 3: return 25;
            case 4: return 50;
            case 5: return 100;
            default:
                return GetCountNeedCardsRecursion(Level - 6) * 100;
        }
    }
    private int GetCountNeedCardsRecursion(int lvl = -1)
    {
        if (lvl == -1)
            lvl = Level;

        switch (lvl)
        {
            case 0: return 2;
            case 1: return 5;
            case 2: return 10;
            case 3: return 25;
            case 4: return 50;
            case 5: return 100;
            default:
                return GetCountNeedCardsRecursion(lvl - 6) * 100;
        }
    }

    public bool IsPosibleLevelUpNow()
    {
        return _countHaveCards>=GetCountNeedCards() && 
            EntryPoint.Instance.CurrencyManager.Bottles.Count>=GetCountNeedBottles();
    }
    public void TryLevelUp()
    {
        if (!IsPosibleLevelUpNow())
            return;

        int consumeCards = GetCountNeedCards();
        double consumeBottles = GetCountNeedBottles();
        Level++;

        _countHaveCards -= consumeCards;
        EntryPoint.Instance.CurrencyManager.Bottles.ChangeCount(-consumeBottles);
    }



    /* Цены прокачки в бутылках.
     * обычная
     * 2 - 50
     * 5 -100
     * 10 -200 
     * 
     * 100 карточек - 800
     * 200 -1500
     * 
     * необычная
     * 5 -200
     * 10 -400
     * 50 - 2000
     * 
     * редкая
     * 2 -500
     * 5 -2000
     * 
     * 
     */
}
