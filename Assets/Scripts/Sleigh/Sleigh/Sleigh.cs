﻿using System;
using UnityEngine;

[RequireComponent(typeof(MoveningTracing))]
[RequireComponent(typeof(SleighAnimator))]
public class Sleigh : MonoBehaviour
{
    private SleighAnimator _animator;
    private MoveningTracing _tracingComponent;
    private WayPoint _currentWayPoint;

    private void Awake()
    {
        _animator = GetComponent<SleighAnimator>();
        _tracingComponent = GetComponent<MoveningTracing>();
    }

    public void MoveTo(Transform position)
    {
        if (position == null)        
            throw new ArgumentNullException($"Position for move is null for {name}");        

        _tracingComponent.SetPositionToMove(position);
        _tracingComponent.RotateToCurrentPosition();

        _tracingComponent.OnStop += Stop;

        _animator.MoveState();
    }

    private void Stop()
    {
        _animator.StopState();

        _tracingComponent.OnStop -= Stop;
    }

    public void SetStartWayPoint(WayPoint wayPoint)
    {
        _currentWayPoint = wayPoint;

        _tracingComponent.SetPositionToMove(wayPoint.transform);
        //_tracingComponent.RotateToCurrentWayPoint();

        _animator.FlyState();

        _tracingComponent.OnStop += SetNewWayPoint;
    }

    private void SetNewWayPoint()
    {
        if (_currentWayPoint.TryGetNextWayPoint(out WayPoint newWayPoint) == true)
        {
            var position = PositionToMove(newWayPoint);
            _tracingComponent.SetPositionToMove(position);
        }
        else
        {
            ActionOnEndTracing();
        }
    }

    private protected virtual Transform PositionToMove(WayPoint newWayPoint)
    {
        _currentWayPoint = newWayPoint;
        return _currentWayPoint.transform;
    }

    private protected virtual void ActionOnEndTracing()
    {
        Destroy(gameObject);
    }

}
