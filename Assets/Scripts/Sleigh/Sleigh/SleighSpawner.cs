using UnityEngine;

public class SleighSpawner : SleighStarterTracing
{
    [SerializeField] private Transform _spawnPosition;

    private void Spawn()
    {
        var sleigh = new InstantiaterPrefab<Sleigh>("Prefabs/Sleigh").Create(_spawnPosition);
        Move(sleigh);
    }

    private void OnMouseDown()
    {
        Spawn();
    }
}

