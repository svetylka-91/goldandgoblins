﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class SleighAnimator : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private ParticleSystem _tail;

    public void StopState()
    {
        _animator.enabled = false;
        _tail.Stop();
    }

    public void MoveState()
    {
        _animator.enabled = true;
        _tail.Stop();
    }

    public void FlyState()
    {
        _animator.enabled = true;
        _tail.Play();
    }
}