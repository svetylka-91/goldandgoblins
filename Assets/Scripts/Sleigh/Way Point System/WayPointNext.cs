﻿using UnityEngine;

public class WayPointNext : WayPoint
{
    [SerializeField] private WayPoint _nextWayPoint;

    public void Awake()
    {
        if (_nextWayPoint == null)
            throw new System.Exception($"Next Way Point is null in {name}");
    }

    public override bool TryGetNextWayPoint(out WayPoint nextWayPoint)
    {
        nextWayPoint = _nextWayPoint;
        return true;
    }

    private void OnValidate()
    {
        if (_nextWayPoint == null)
            Debug.LogWarning($"Next Way Point is null in {name}");
    }
}
