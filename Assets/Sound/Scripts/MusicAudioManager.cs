using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicAudioManager : MonoBehaviour
{
    public static MusicAudioManager instance = null;

    [SerializeField] private AudioSource _musicMainSource;
    [SerializeField] private AudioSource _musicRewardflowSource;

    [SerializeField] private AudioClip _musicMainSound;
    [SerializeField] private AudioClip _musicRewardflowSound;


    private static AudioSource s_musicMainSource;
    private static AudioSource s_musicRewardflowSource;

    private static AudioClip s_musicMainSound;
    private static AudioClip s_musicRewardflowSound;

    private void Start()
    {
        InitializeManager();
        DontDestroyOnLoad(gameObject);
    }

    public static void PlayMainSound()
    {
        s_musicMainSource.PlayOneShot(s_musicMainSound);
    }

    public static void PlayMusicRewardflowSound()
    {
        s_musicRewardflowSource.PlayOneShot(s_musicRewardflowSound);
    }

    private void InitializeManager()
    {
        s_musicMainSource = _musicMainSource;
        s_musicRewardflowSource = _musicRewardflowSource;

        s_musicMainSound = _musicMainSound;
        s_musicRewardflowSound = _musicRewardflowSound;
    }
} 
