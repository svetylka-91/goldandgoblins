using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMangers : MonoBehaviour
{

    [SerializeField] private GameObject _uiAudioManager;
    [SerializeField] private GameObject _chestOpeningManager;
    [SerializeField] private GameObject _effectsAudioManager;
    [SerializeField] private GameObject _goblinAudioManager;

    private void Awake()
    {
        if (UIAudioManager.instance == null)
            Instantiate(_uiAudioManager);

        if (ChestOpeningManager.instance == null)
            Instantiate(_chestOpeningManager);

        if (EffectsAudioManager.instance == null)
            Instantiate(_effectsAudioManager);

        if (GoblinAudioManager.instance == null)
            Instantiate(_goblinAudioManager);
    }
}
